/*==============================================================================
 * ETMR0 настроен на прерывание каждую  1 мс. В прерывании переключается
 * состояние вывода GPIOA[7], таким образом на данном выводе генерерируется
 * меандр с периодом в 2 мс.
 *------------------------------------------------------------------------------
 * НИИЭТ, Богдан Колбов <kolbov@niiet.ru>
 *==============================================================================
 * ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО
 * ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ ГАРАНТИИ ТОВАРНОЙ
 * ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ НАЗНАЧЕНИЮ И ОТСУТСТВИЯ
 * НАРУШЕНИЙ, НО НЕ ОГРАНИЧИВАЯСЬ ИМИ. ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ
 * ПРЕДНАЗНАЧЕНО ДЛЯ ОЗНАКОМИТЕЛЬНЫХ ЦЕЛЕЙ И НАПРАВЛЕНО ТОЛЬКО НА
 * ПРЕДОСТАВЛЕНИЕ ДОПОЛНИТЕЛЬНОЙ ИНФОРМАЦИИ О ПРОДУКТЕ, С ЦЕЛЬЮ СОХРАНИТЬ ВРЕМЯ
 * ПОТРЕБИТЕЛЮ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ
 * ОТВЕТСТВЕННОСТИ ПО КАКИМ-ЛИБО ИСКАМ, ЗА ПРЯМОЙ ИЛИ КОСВЕННЫЙ УЩЕРБ, ИЛИ
 * ПО ИНЫМ ТРЕБОВАНИЯМ, ВОЗНИКШИМ ИЗ-ЗА ИСПОЛЬЗОВАНИЯ ПРОГРАММНОГО ОБЕСПЕЧЕНИЯ
 * ИЛИ ИНЫХ ДЕЙСТВИЙ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.
 *
 *                              2019 АО "НИИЭТ"
 *==============================================================================
 */

//-- Includes ------------------------------------------------------------------
#include "plib028.h"
#include "retarget_conf.h"

//-- Defines -------------------------------------------------------------------

//-- Peripheral init functions -------------------------------------------------
void gpio_init()
{
    RCU_AHBClkCmd(RCU_AHBClk_GPIOA, ENABLE);
    RCU_AHBRstCmd(RCU_AHBRst_GPIOA, ENABLE);
    GPIO_OutCmd(GPIOA, GPIO_Pin_7_0, ENABLE);
    GPIO_DigitalCmd(GPIOA, GPIO_Pin_7_0, ENABLE);
    GPIO_SetBits(GPIOA, GPIO_Pin_7_0);
}

void etmr_init()
{
    // инициализация таймера
    RCU_APBClk0Cmd(RCU_APBClk0_ETMR0, ENABLE);
    RCU_APBRst0Cmd(RCU_APBRst0_ETMR0, ENABLE);
    ETMR_ModeConfig(ETMR0,ETMR_Mode_Periodic);
    ETMR_SizeConfig(ETMR0, ETMR_Size_32bit);
    ETMR_PeriodConfig(ETMR0, APB0BusClock, 1000);
    ETMR_ITCmd(ETMR0, ENABLE);
    ETMR_Cmd(ETMR0, ENABLE);
    NVIC_EnableIRQ(ETMR0_IRQn);
}

void periph_init()
{
    SystemCoreClockUpdate();
    retarget_init();
    gpio_init();
    etmr_init();
    printf("\nAll peripherals inited, SYSCLK = %3d MHz\n", (int)(SystemCoreClock / 1E6));
}

//-- Main ----------------------------------------------------------------------
int main()
{
    periph_init();

    while (1) {
    };
    return 0;
}

//-- IRQ handlers --------------------------------------------------------------
void ETMR0_IRQHandler()
{
    ETMR_ITStatusClear(ETMR0);
    GPIO_ToggleBits(GPIOA, GPIO_Pin_7);
}

//-- Assert --------------------------------------------------------------------
#if defined USE_FULL_ASSERT
void assert_failed(uint8_t* file, uint32_t line)
{
    printf("Assert failed: file %s on line %d\n", file, (int)line);
    while (1) {
    };
}
#endif /* USE_FULL_ASSERT */
