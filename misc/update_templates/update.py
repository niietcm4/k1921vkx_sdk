#!/user/bin/env python3

"""
Script to update template projects
"""

import sys
import shutil
import os


def copytree(src, dst, symlinks=False, ignore=None):
    if not os.path.exists(dst):
        os.makedirs(dst)
    for item in os.listdir(src):
        s = os.path.join(src, item)
        d = os.path.join(dst, item)
        if os.path.isdir(s):
            copytree(s, d, symlinks, ignore)
        else:
            if not os.path.exists(d) or os.stat(s).st_mtime - os.stat(d).st_mtime > 1:
                shutil.copy2(s, d)


if sys.version_info < (3, 5, 0):
    print("You need python 3.5.0 or later to run this script!")
    exit(1)


# -- k1921vk035-bare --
k1921vk035_bare_path = '../../templates/k1921vk035-bare'
copytree('../../platform/Device/NIIET/K1921VK035', k1921vk035_bare_path + '/platform/Device/NIIET/K1921VK035')
copytree('../../platform/CMSIS', k1921vk035_bare_path + '/platform/CMSIS')
copytree('../../platform/retarget/Template/K1921VK035', k1921vk035_bare_path + '/platform/retarget/Template/K1921VK035')
shutil.copy('../../platform/retarget/retarget.c', k1921vk035_bare_path + '/platform/retarget/')
shutil.copy('../../tools/gcc_cm4f.cmake', k1921vk035_bare_path + '/GCC/')

# -- k1921vk035-plib035 --
k1921vk035_plib035_path = '../../templates/k1921vk035-plib035'
copytree('../../platform/Device/NIIET/K1921VK035', k1921vk035_plib035_path + '/platform/Device/NIIET/K1921VK035')
copytree('../../platform/CMSIS', k1921vk035_plib035_path + '/platform/CMSIS')
copytree('../../platform/plib035/inc', k1921vk035_plib035_path + '/platform/plib035/inc')
copytree('../../platform/plib035/src', k1921vk035_plib035_path + '/platform/plib035/src')
copytree('../../platform/retarget/Template/K1921VK035', k1921vk035_plib035_path + '/platform/retarget/Template/K1921VK035')
shutil.copy('../../platform/retarget/retarget.c', k1921vk035_plib035_path + '/platform/retarget/')
shutil.copy('../../tools/gcc_cm4f.cmake', k1921vk035_plib035_path + '/GCC/')

# -- k1921vk01t-bare --
k1921vk01t_bare_path = '../../templates/k1921vk01t-bare'
copytree('../../platform/Device/NIIET/K1921VK01T', k1921vk01t_bare_path + '/platform/Device/NIIET/K1921VK01T')
copytree('../../platform/CMSIS', k1921vk01t_bare_path + '/platform/CMSIS')
copytree('../../platform/retarget/Template/K1921VK01T', k1921vk01t_bare_path + '/platform/retarget/Template/K1921VK01T')
shutil.copy('../../platform/retarget/retarget.c', k1921vk01t_bare_path + '/platform/retarget/')
shutil.copy('../../tools/gcc_cm4f.cmake', k1921vk01t_bare_path + '/GCC/')

# -- k1921vk01t-niietcm4pd --
k1921vk01t_niietcm4pd_path = '../../templates/k1921vk01t-niietcm4pd'
copytree('../../platform/Device/NIIET/K1921VK01T', k1921vk01t_niietcm4pd_path + '/platform/Device/NIIET/K1921VK01T')
copytree('../../platform/CMSIS', k1921vk01t_niietcm4pd_path + '/platform/CMSIS')
copytree('../../platform/niietcm4_pd/inc', k1921vk01t_niietcm4pd_path + '/platform/niietcm4_pd/inc')
copytree('../../platform/niietcm4_pd/src', k1921vk01t_niietcm4pd_path + '/platform/niietcm4_pd/src')
copytree('../../platform/retarget/Template/K1921VK01T', k1921vk01t_niietcm4pd_path + '/platform/retarget/Template/K1921VK01T')
shutil.copy('../../platform/retarget/retarget.c', k1921vk01t_niietcm4pd_path + '/platform/retarget/')
shutil.copy('../../tools/gcc_cm4f.cmake', k1921vk01t_niietcm4pd_path + '/GCC/')

# -- k1921vk028-bare --
k1921vk028_bare_path = '../../templates/k1921vk028-bare'
copytree('../../platform/Device/NIIET/K1921VK028', k1921vk028_bare_path + '/platform/Device/NIIET/K1921VK028')
copytree('../../platform/CMSIS', k1921vk028_bare_path + '/platform/CMSIS')
copytree('../../platform/retarget/Template/K1921VK028', k1921vk028_bare_path + '/platform/retarget/Template/K1921VK028')
shutil.copy('../../platform/retarget/retarget.c', k1921vk028_bare_path + '/platform/retarget/')
shutil.copy('../../tools/gcc_cm4f.cmake', k1921vk028_bare_path + '/GCC/')

# -- k1921vk028-plib028 --
k1921vk028_plib028_path = '../../templates/k1921vk028-plib028'
copytree('../../platform/Device/NIIET/K1921VK028', k1921vk028_plib028_path + '/platform/Device/NIIET/K1921VK028')
copytree('../../platform/CMSIS', k1921vk028_plib028_path + '/platform/CMSIS')
copytree('../../platform/plib028/inc', k1921vk028_plib028_path + '/platform/plib028/inc')
copytree('../../platform/plib028/src', k1921vk028_plib028_path + '/platform/plib028/src')
copytree('../../platform/retarget/Template/K1921VK028', k1921vk028_plib028_path + '/platform/retarget/Template/K1921VK028')
shutil.copy('../../platform/retarget/retarget.c', k1921vk028_plib028_path + '/platform/retarget/')
shutil.copy('../../tools/gcc_cm4f.cmake', k1921vk028_plib028_path + '/GCC/')
