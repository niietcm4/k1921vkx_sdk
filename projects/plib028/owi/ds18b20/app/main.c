/*==============================================================================
 * Получение значения температуры сдатчика DS18B20 с помощью
 * однопроводного интерфейса OWI каждые 2 секунды
 *------------------------------------------------------------------------------
 * НИИЭТ, Богдан Колбов <kolbov@niiet.ru>
 *==============================================================================
 * ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО
 * ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ ГАРАНТИИ ТОВАРНОЙ
 * ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ НАЗНАЧЕНИЮ И ОТСУТСТВИЯ
 * НАРУШЕНИЙ, НО НЕ ОГРАНИЧИВАЯСЬ ИМИ. ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ
 * ПРЕДНАЗНАЧЕНО ДЛЯ ОЗНАКОМИТЕЛЬНЫХ ЦЕЛЕЙ И НАПРАВЛЕНО ТОЛЬКО НА
 * ПРЕДОСТАВЛЕНИЕ ДОПОЛНИТЕЛЬНОЙ ИНФОРМАЦИИ О ПРОДУКТЕ, С ЦЕЛЬЮ СОХРАНИТЬ ВРЕМЯ
 * ПОТРЕБИТЕЛЮ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ
 * ОТВЕТСТВЕННОСТИ ПО КАКИМ-ЛИБО ИСКАМ, ЗА ПРЯМОЙ ИЛИ КОСВЕННЫЙ УЩЕРБ, ИЛИ
 * ПО ИНЫМ ТРЕБОВАНИЯМ, ВОЗНИКШИМ ИЗ-ЗА ИСПОЛЬЗОВАНИЯ ПРОГРАММНОГО ОБЕСПЕЧЕНИЯ
 * ИЛИ ИНЫХ ДЕЙСТВИЙ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.
 *
 *                              2019 АО "НИИЭТ"
 *==============================================================================
 */

//-- Includes ------------------------------------------------------------------
#include "plib028.h"
#include "retarget_conf.h"

//-- Defines -------------------------------------------------------------------

//-- Variables -----------------------------------------------------------------
volatile uint32_t ds18b20_fsm = 0;

struct {
    union {
        volatile uint16_t value16;
        struct {
            volatile uint16_t f : 4;
            volatile uint16_t i : 12;
        } value;
        volatile uint8_t value8[2];
    } temp;
} ds18b20;

//-- Special peripheral functions ----------------------------------------------
uint32_t OWI_ResetLine(void)
{
    OWI_RstCmd(OWI0);
    while (OWI_BusyStatus(OWI0)) {
    };
    return (uint32_t)(OWI_PresentStatus(OWI0));
}

void OWI_Tx(uint8_t data)
{
    OWI_SendData(OWI0, data);
    while (OWI_BusyStatus(OWI0)) {
    };
}

uint8_t OWI_Rx(void)
{
    OWI_ReadCmd(OWI0);
    while (OWI_BusyStatus(OWI0)) {
    };
    return (uint8_t)OWI_ReceiveData(OWI0);
}

//-- Peripheral init functions -------------------------------------------------
void gpio_init()
{
    RCU_AHBClkCmd(RCU_AHBClk_GPIOA, ENABLE);
    RCU_AHBRstCmd(RCU_AHBRst_GPIOA, ENABLE);
    GPIO_OutCmd(GPIOA, GPIO_Pin_7_0, ENABLE);
    GPIO_DigitalCmd(GPIOA, GPIO_Pin_7_0, ENABLE);
    GPIO_SetBits(GPIOA, GPIO_Pin_7_0);
}

void owi_init()
{
    RCU_AHBClkCmd(RCU_AHBClk_GPIOD, ENABLE);
    RCU_AHBRstCmd(RCU_AHBRst_GPIOD, ENABLE);
    GPIO_Init_TypeDef GPIO_InitStruct;
    GPIO_StructInit(&GPIO_InitStruct);
    GPIO_InitStruct.AltFuncNum = GPIO_AltFuncNum_1;
    GPIO_InitStruct.AltFunc = ENABLE;
    GPIO_InitStruct.PullMode = GPIO_PullMode_PU;
    GPIO_InitStruct.Pin = GPIO_Pin_0;
    GPIO_Init(GPIOD, &GPIO_InitStruct);
    GPIO_DigitalCmd(GPIOD, GPIO_Pin_0, ENABLE);

    RCU_APBClk1Cmd(RCU_APBClk1_OWI0, ENABLE);
    RCU_APBRst1Cmd(RCU_APBRst1_OWI0, ENABLE);
    OWI_TimingConfig(OWI0, APB0BusClock);
    OWI_Cmd(OWI0, ENABLE);
}

void periph_init()
{
    SystemCoreClockUpdate();
    retarget_init();
    gpio_init();
    owi_init();
    printf("\nAll peripherals inited, SYSCLK = %3d MHz\n", (int)(SystemCoreClock / 1E6));
}

//-- Main ----------------------------------------------------------------------
int main()
{
    periph_init();
    SysTick_Config(SystemCoreClock / 1000 - 1); // 1ms tick

    while (1) {
        switch (ds18b20_fsm) {
        case 1: //in idle state
            if (OWI_ResetLine() != 0) {
                OWI_Tx(0xCC); // skip rom
                OWI_Tx(0x44); // conv t
            } else
                GPIO_ToggleBits(GPIOA, GPIO_Pin_1);
            break;
        case 2: // read conv temp
            if (OWI_ResetLine() != 0) {
                OWI_Tx(0xCC); // skip rom
                OWI_Tx(0xBE); // read scratchpad

                ds18b20.temp.value8[0] = OWI_Rx();
                ds18b20.temp.value8[1] = OWI_Rx();
                ds18b20_fsm = 0;

                float temp = ds18b20.temp.value.f ? (float)(ds18b20.temp.value.i + 1.0f / ds18b20.temp.value.f) : (float)(ds18b20.temp.value.i);
                printf("Temperature = %f (0x%04x)\n", temp, ds18b20.temp.value16);
            } else
                GPIO_ToggleBits(GPIOA, GPIO_Pin_1);
            ds18b20_fsm = 0;
            break;
        }
    };
    return 0;
}

//-- IRQ handlers --------------------------------------------------------------
volatile uint32_t tick_counter = 0;
void SysTick_Handler()
{
    if (tick_counter < 1000)
        tick_counter++;
    else {
        ds18b20_fsm++;
        tick_counter = 0;
        GPIO_ToggleBits(GPIOA, GPIO_Pin_0);
    }
}

//-- Assert --------------------------------------------------------------------
#if defined USE_FULL_ASSERT
void assert_failed(uint8_t* file, uint32_t line)
{
    printf("Assert failed: file %s on line %d\n", file, (int)line);
    while (1) {
    };
}
#endif /* USE_FULL_ASSERT */
