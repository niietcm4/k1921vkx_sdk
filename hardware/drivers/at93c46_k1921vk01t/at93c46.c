/*==============================================================================
 * Функции для работы с EEPROM AT93C46 через SPI микроконтроллера К1921ВК01Т.
 * Простая реализация с использованием программных задержек ожидания выполнения
 * операции. CS управляется вручную, как GPIO.
 *------------------------------------------------------------------------------
 * НИИЭТ, Богдан Колбов <kolbov@niiet.ru>
 *==============================================================================
 * ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО
 * ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ ГАРАНТИИ ТОВАРНОЙ
 * ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ НАЗНАЧЕНИЮ И ОТСУТСТВИЯ
 * НАРУШЕНИЙ, НО НЕ ОГРАНИЧИВАЯСЬ ИМИ. ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ
 * ПРЕДНАЗНАЧЕНО ДЛЯ ОЗНАКОМИТЕЛЬНЫХ ЦЕЛЕЙ И НАПРАВЛЕНО ТОЛЬКО НА
 * ПРЕДОСТАВЛЕНИЕ ДОПОЛНИТЕЛЬНОЙ ИНФОРМАЦИИ О ПРОДУКТЕ, С ЦЕЛЬЮ СОХРАНИТЬ ВРЕМЯ
 * ПОТРЕБИТЕЛЮ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ
 * ОТВЕТСТВЕННОСТИ ПО КАКИМ-ЛИБО ИСКАМ, ЗА ПРЯМОЙ ИЛИ КОСВЕННЫЙ УЩЕРБ, ИЛИ
 * ПО ИНЫМ ТРЕБОВАНИЯМ, ВОЗНИКШИМ ИЗ-ЗА ИСПОЛЬЗОВАНИЯ ПРОГРАММНОГО ОБЕСПЕЧЕНИЯ
 * ИЛИ ИНЫХ ДЕЙСТВИЙ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.
 *
 *                              2018 АО "НИИЭТ"
 *==============================================================================
 */

//-- Includes ------------------------------------------------------------------
#include "at93c46.h"
#include "system_K1921VK01T.h"

//-- Private Defines -----------------------------------------------------------
#define SPI_CS_PIN_Msk (1 << 8)
#define SPI_MISO_PIN_Msk (1 << 2)
#define SPI_MOSI_PIN_Msk (1 << 0)
#define SPI_SCK_PIN_Msk (1 << 9)

#define AT93C46_OPCODE_READ ((uint16_t)0x0180)
#define AT93C46_OPCODE_WEN ((uint16_t)0x9800)
#define AT93C46_OPCODE_WRITE ((uint16_t)0x0140)
#define AT93C46_OPCODE_WRALL ((uint16_t)0x0110)
#define AT93C46_OPCODE_WDS ((uint16_t)0x0100)
#define AT93C46_OPCODE_ERASE ((uint16_t)0x01C0)
#define AT93C46_OPCODE_ERAL ((uint16_t)0x0120)

//-- Private Types -------------------------------------------------------------

//-- Private Functions ---------------------------------------------------------
__STATIC_INLINE void SetCS(void)
{
    NT_GPIOE->DATAOUT |= SPI_CS_PIN_Msk;
}

__STATIC_INLINE void ClrCS()
{
    NT_GPIOE->DATAOUT &= ~SPI_CS_PIN_Msk;
}

static void EnableDelay(void)
{
    CoreDebug->DEMCR = CoreDebug_DEMCR_TRCENA_Msk;
    DWT->CYCCNT = 0;
    DWT->CTRL = DWT_CTRL_CYCCNTENA_Msk;
}

//Not for precision delays
static void Delay(uint32_t us)
{
    uint32_t ticks = us * (SystemCoreClock / 1000000);
    DWT->CYCCNT = 0;
    while (DWT->CYCCNT < ticks) {
    };
}

//-- Functions -----------------------------------------------------------------
void AT93C46_Init()
{
    NT_COMMON_REG->UART_SPI_CLK_SEL_bit.SEL_SPI1 = 0; //SYSCLK=100MHz
    NT_COMMON_REG->SPI_CLK_bit.CLKEN_SPI1 = 1;
    NT_COMMON_REG->PER_RST0_bit.SPIRST1 = 1;

    NT_SPI1->SPI_CPSR_bit.CPSDVSR = 50;
    NT_SPI1->SPI_CR0_bit.SCR = 9;  // SCK=100MHz/((9+1)*50)=200kHz
    NT_SPI1->SPI_CR0_bit.FRF = 0;   //SPI Motorola mode
    NT_SPI1->SPI_CR0_bit.DSS = 0xF; // Data size 16 bit
    NT_SPI1->SPI_CR1_bit.SSE = 1;

    //IO config
    NT_COMMON_REG->GPIOPCTLG_bit.PIN2 = 2; //af3 G2 (MISO)
    NT_COMMON_REG->GPIOPCTLF_bit.PIN0 = 2; //af3 F0 (MOSI)
    NT_COMMON_REG->GPIOPCTLE_bit.PIN9 = 2; //af3 E9 (SCK)
    NT_GPIOE->ALTFUNCCLR = SPI_CS_PIN_Msk;
    NT_GPIOE->OUTENSET = SPI_CS_PIN_Msk;
    NT_GPIOG->ALTFUNCSET = SPI_MISO_PIN_Msk;
    NT_GPIOF->ALTFUNCSET = SPI_MOSI_PIN_Msk;
    NT_GPIOE->ALTFUNCSET = SPI_SCK_PIN_Msk;

    NT_COMMON_REG->GPIODENE |= SPI_CS_PIN_Msk;
    NT_COMMON_REG->GPIODENG |= SPI_MISO_PIN_Msk;
    NT_COMMON_REG->GPIODENF |= SPI_MOSI_PIN_Msk;
    NT_COMMON_REG->GPIODENE |= SPI_SCK_PIN_Msk;

    EnableDelay();
}

void AT93C46_WriteEnable(void)
{
    SetCS();
    NT_SPI1->SPI_DR = AT93C46_OPCODE_WEN;
    while (NT_SPI1->SPI_SR_bit.BSY) {
    };
    ClrCS();
    (void)NT_SPI1->SPI_DR; //dummy read
    Delay(1);              //min 1us delay
}

void AT93C46_WriteDisable(void)
{
    SetCS();
    NT_SPI1->SPI_DR = AT93C46_OPCODE_WDS;
    while (NT_SPI1->SPI_SR_bit.BSY) {
    };
    ClrCS();
    (void)NT_SPI1->SPI_DR; //dummy read
    Delay(1);              //min 1us delay
}

uint32_t AT93C46_Write(uint16_t Addr, uint16_t Data)
{
    if (Addr < AT93C46_WORDS_TOTAL) {
        SetCS();
        NT_SPI1->SPI_DR = AT93C46_OPCODE_WRITE | Addr;
        NT_SPI1->SPI_DR = Data;
        while (NT_SPI1->SPI_SR_bit.BSY) {
        };
        ClrCS();
        (void)NT_SPI1->SPI_DR; //dummy read
        (void)NT_SPI1->SPI_DR; //dummy read
        Delay(15000);          //min 15ms delay
        return 0;
    } else
        return 1;
}

void AT93C46_WriteAll(uint16_t Data)
{
    SetCS();
    NT_SPI1->SPI_DR = AT93C46_OPCODE_WRALL;
    NT_SPI1->SPI_DR = Data;
    while (NT_SPI1->SPI_SR_bit.BSY) {
    };
    ClrCS();
    (void)NT_SPI1->SPI_DR; //dummy read
    (void)NT_SPI1->SPI_DR; //dummy read
    Delay(15000);          //min 15ms delay
}

uint16_t AT93C46_Read(uint16_t Addr)
{
    uint16_t buf = 0;
    if (Addr < AT93C46_WORDS_TOTAL) {
        SetCS();
        NT_SPI1->SPI_DR = AT93C46_OPCODE_READ | Addr;
        NT_SPI1->SPI_DR = 0x0000;
        NT_SPI1->SPI_DR = 0x0000;
        while (NT_SPI1->SPI_SR_bit.BSY) {
        };
        ClrCS();
        (void)NT_SPI1->SPI_DR;
        buf = NT_SPI1->SPI_DR;
        buf = (buf << 1) | (NT_SPI1->SPI_DR >> 15);
        Delay(1); //min 1us delay
    }
    return buf;
}

uint32_t AT93C46_Erase(uint16_t Addr)
{
    if (Addr < AT93C46_WORDS_TOTAL) {
        SetCS();
        NT_SPI1->SPI_DR = AT93C46_OPCODE_ERASE | Addr;
        while (NT_SPI1->SPI_SR_bit.BSY) {
        };
        ClrCS();
        (void)NT_SPI1->SPI_DR;
        Delay(15000); //min 15ms delay
        return 0;
    } else
        return 1;
}

void AT93C46_EraseAll(void)
{
    SetCS();
    NT_SPI1->SPI_DR = AT93C46_OPCODE_ERAL;
    while (NT_SPI1->SPI_SR_bit.BSY) {
    };
    ClrCS();
    (void)NT_SPI1->SPI_DR;
    Delay(15000); //min 15ms delay
}
