/*==============================================================================
 * Тест режима авто-запрос работы DMA.
 * Пересылка 8-битных значений из массива в массив в режиме автозапроса
 * по первым 4 каналам.
 *------------------------------------------------------------------------------
 * НИИЭТ, Богдан Колбов <kolbov@niiet.ru>
 *==============================================================================
 * ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО
 * ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ ГАРАНТИИ ТОВАРНОЙ
 * ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ НАЗНАЧЕНИЮ И ОТСУТСТВИЯ
 * НАРУШЕНИЙ, НО НЕ ОГРАНИЧИВАЯСЬ ИМИ. ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ
 * ПРЕДНАЗНАЧЕНО ДЛЯ ОЗНАКОМИТЕЛЬНЫХ ЦЕЛЕЙ И НАПРАВЛЕНО ТОЛЬКО НА
 * ПРЕДОСТАВЛЕНИЕ ДОПОЛНИТЕЛЬНОЙ ИНФОРМАЦИИ О ПРОДУКТЕ, С ЦЕЛЬЮ СОХРАНИТЬ ВРЕМЯ
 * ПОТРЕБИТЕЛЮ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ
 * ОТВЕТСТВЕННОСТИ ПО КАКИМ-ЛИБО ИСКАМ, ЗА ПРЯМОЙ ИЛИ КОСВЕННЫЙ УЩЕРБ, ИЛИ
 * ПО ИНЫМ ТРЕБОВАНИЯМ, ВОЗНИКШИМ ИЗ-ЗА ИСПОЛЬЗОВАНИЯ ПРОГРАММНОГО ОБЕСПЕЧЕНИЯ
 * ИЛИ ИНЫХ ДЕЙСТВИЙ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.
 *
 *                              2018 АО "НИИЭТ"
 *==============================================================================
 */

//-- Includes ------------------------------------------------------------------
#include "plib035.h"
#include "retarget_conf.h"

//-- Defines -------------------------------------------------------------------
#define CHANNELS_TOTAL 4
#define TRANSFERS_TOTAL 8

//-- Variables -----------------------------------------------------------------
DMA_CtrlData_TypeDef DMA_CTRLDATA __ALIGNED(512);
uint8_t array_src[CHANNELS_TOTAL][TRANSFERS_TOTAL];
uint8_t array_dst[CHANNELS_TOTAL][TRANSFERS_TOTAL];
volatile uint32_t irq_status = 0;

//-- Peripheral init functions -------------------------------------------------
void dma_init()
{
    /* Инициализация базового адреса */
    DMA_BasePtrConfig((uint32_t)(&DMA_CTRLDATA));
    /* Инициализация каналов */
    DMA_ChannelInit_TypeDef DMA_ChannelInitStruct;
    DMA_ChannelStructInit(&DMA_ChannelInitStruct);
    DMA_ChannelInitStruct.SrcDataSize = DMA_DataSize_8;
    DMA_ChannelInitStruct.SrcDataInc = DMA_DataInc_8;
    DMA_ChannelInitStruct.DstDataSize = DMA_DataSize_8;
    DMA_ChannelInitStruct.DstDataInc = DMA_DataInc_8;
    DMA_ChannelInitStruct.ArbitrationRate = DMA_ArbitrationRate_1;
    DMA_ChannelInitStruct.TransfersTotal = TRANSFERS_TOTAL;
    DMA_ChannelInitStruct.Mode = DMA_Mode_AutoReq;
    for (uint32_t ch = 0; ch < CHANNELS_TOTAL; ch++) {
        DMA_ChannelInitStruct.SrcDataEndPtr = (uint32_t*)((uint32_t)(&array_src[ch][TRANSFERS_TOTAL - 1]) | 0x3);
        DMA_ChannelInitStruct.DstDataEndPtr = (uint32_t*)((uint32_t)(&array_dst[ch][TRANSFERS_TOTAL - 1]) | 0x3);
        DMA_ChannelInit(&DMA_CTRLDATA.PRM_DATA.CH[ch], &DMA_ChannelInitStruct);
    }

    /* Иницализация контроллера */
    DMA_Init_TypeDef DMA_InitStruct;
    DMA_StructInit(&DMA_InitStruct);
    DMA_InitStruct.Channel = DMA_Channel_0 | DMA_Channel_1 | DMA_Channel_2 | DMA_Channel_3;
    DMA_InitStruct.ChannelEnable = ENABLE;
    DMA_Init(&DMA_InitStruct);
    DMA_MasterEnableCmd(ENABLE);

    /* NVIC init */
    NVIC_EnableIRQ(DMA_CH0_IRQn);
    NVIC_EnableIRQ(DMA_CH1_IRQn);
    NVIC_EnableIRQ(DMA_CH2_IRQn);
    NVIC_EnableIRQ(DMA_CH3_IRQn);
}
void periph_init()
{
    SystemCoreClockUpdate();
    retarget_init();
    dma_init();
    printf("\nAll peripherals inited, SYSCLK = %3d MHz\n", (int)(SystemCoreClock / 1E6));
}

//-- Main ----------------------------------------------------------------------
int main()
{
    periph_init();

    /* инициализация массивов */
    for (uint32_t chan = 0; chan < CHANNELS_TOTAL; chan++) {
        for (uint32_t elem = 0; elem < TRANSFERS_TOTAL; elem++) {
            array_src[chan][elem] = 0xA0 + chan;
            array_dst[chan][elem] = 0xE0 + chan;
        }
    }

    /* начинаем пересылку */
    DMA_SwRequestCmd(DMA_Channel_0 | DMA_Channel_1 | DMA_Channel_2 | DMA_Channel_3);

    /* ждем пока dma выполняет запросы */
    while (irq_status != (DMA_Channel_0 | DMA_Channel_1 | DMA_Channel_2 | DMA_Channel_3)) {
    };

    /* проверяем результаты */
    uint32_t check_error = 0;
    for (uint32_t ch = 0; ch < CHANNELS_TOTAL; ch++) {
        for (uint32_t i = 0; i < TRANSFERS_TOTAL; i++) {
            printf("array_dst[CH%d][%d]=0x%02x; array_src[CH%d][%d]=0x%02x\n",
                   (int)ch, (int)i,
                   (unsigned int)array_dst[ch][i],
                   (int)ch, (int)i,
                   (unsigned int)array_src[ch][i]);
            if (array_src[ch][i] != array_dst[ch][i])
                check_error++;
        }
    }

    printf("All transfers done with %d errors!\n", (int)check_error);

    while (1) {
    };

    return 0;
}

//-- IRQ handlers --------------------------------------------------------------
void DMA_CH0_IRQHandler()
{
    irq_status |= DMA_Channel_0;
}
void DMA_CH1_IRQHandler()
{
    irq_status |= DMA_Channel_1;
}
void DMA_CH2_IRQHandler()
{
    irq_status |= DMA_Channel_2;
}
void DMA_CH3_IRQHandler()
{
    irq_status |= DMA_Channel_3;
}

//-- Assert --------------------------------------------------------------------
#if defined USE_FULL_ASSERT
void assert_failed(uint8_t* file, uint32_t line)
{
    printf("Assert failed: file %s on line %d\n", file, (int)line);
    while (1) {
    };
}
#endif /* USE_FULL_ASSERT */
