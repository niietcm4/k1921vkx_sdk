/*==============================================================================
 * Функции для работы с EEPROM AT93C46 через SPI микроконтроллера К1921ВК035.
 * Простая реализация с использованием программных задержек ожидания выполнения
 * операции. CS управляется вручную, как GPIO.
 *------------------------------------------------------------------------------
 * НИИЭТ, Богдан Колбов <kolbov@niiet.ru>
 *==============================================================================
 * ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО
 * ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ ГАРАНТИИ ТОВАРНОЙ
 * ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ НАЗНАЧЕНИЮ И ОТСУТСТВИЯ
 * НАРУШЕНИЙ, НО НЕ ОГРАНИЧИВАЯСЬ ИМИ. ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ
 * ПРЕДНАЗНАЧЕНО ДЛЯ ОЗНАКОМИТЕЛЬНЫХ ЦЕЛЕЙ И НАПРАВЛЕНО ТОЛЬКО НА
 * ПРЕДОСТАВЛЕНИЕ ДОПОЛНИТЕЛЬНОЙ ИНФОРМАЦИИ О ПРОДУКТЕ, С ЦЕЛЬЮ СОХРАНИТЬ ВРЕМЯ
 * ПОТРЕБИТЕЛЮ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ
 * ОТВЕТСТВЕННОСТИ ПО КАКИМ-ЛИБО ИСКАМ, ЗА ПРЯМОЙ ИЛИ КОСВЕННЫЙ УЩЕРБ, ИЛИ
 * ПО ИНЫМ ТРЕБОВАНИЯМ, ВОЗНИКШИМ ИЗ-ЗА ИСПОЛЬЗОВАНИЯ ПРОГРАММНОГО ОБЕСПЕЧЕНИЯ
 * ИЛИ ИНЫХ ДЕЙСТВИЙ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.
 *
 *                              2018 АО "НИИЭТ"
 *==============================================================================
 */

//-- Includes ------------------------------------------------------------------
#include "at93c46.h"

//-- Private Defines -----------------------------------------------------------
#define AT93C46_OPCODE_READ ((uint16_t)0x0180)
#define AT93C46_OPCODE_WEN ((uint16_t)0x9800)
#define AT93C46_OPCODE_WRITE ((uint16_t)0x0140)
#define AT93C46_OPCODE_WRALL ((uint16_t)0x0110)
#define AT93C46_OPCODE_WDS ((uint16_t)0x0100)
#define AT93C46_OPCODE_ERASE ((uint16_t)0x01C0)
#define AT93C46_OPCODE_ERAL ((uint16_t)0x0120)

//-- Private Types -------------------------------------------------------------

//-- Private Functions ---------------------------------------------------------
__STATIC_INLINE void SetCS(void)
{
    GPIOB->DATAOUTSET = GPIO_DATAOUTSET_PIN4_Msk;
}

__STATIC_INLINE void ClrCS()
{
    GPIOB->DATAOUTCLR = GPIO_DATAOUTSET_PIN4_Msk;
}

static void EnableDelay(void)
{
    CoreDebug->DEMCR = CoreDebug_DEMCR_TRCENA_Msk;
    DWT->CYCCNT = 0;
    DWT->CTRL = DWT_CTRL_CYCCNTENA_Msk;
}

//Not for precision delays
static void Delay(uint32_t us)
{
    uint32_t ticks = us * (SystemCoreClock / 1000000);
    DWT->CYCCNT = 0;
    while (DWT->CYCCNT < ticks) {
    };
}

//-- Functions -----------------------------------------------------------------
void AT93C46_Init()
{
    RCU->SPICFG_bit.CLKSEL = RCU_SPICFG_CLKSEL_PLLCLK;
    RCU->SPICFG_bit.CLKEN = 1;
    RCU->SPICFG_bit.RSTDIS = 1;

    SPI->CPSR_bit.CPSDVSR = 50;
    SPI->CR0_bit.SCR = 9; // SCK=100MHz/((9+1)*50)=200kHz
    SPI->CR0_bit.FRF = SPI_CR0_FRF_SPI;
    SPI->CR0_bit.DSS = SPI_CR0_DSS_16bit;
    SPI->CR1_bit.SSE = 1;

    RCU->HCLKCFG_bit.GPIOBEN = 1;
    RCU->HRSTCFG_bit.GPIOBEN = 1;
    //IO config
    GPIOB->ALTFUNCSET = (GPIO_ALTFUNCSET_PIN5_Msk |
                         GPIO_ALTFUNCSET_PIN6_Msk |
                         GPIO_ALTFUNCSET_PIN7_Msk);
    GPIOB->OUTENSET = GPIO_OUTENSET_PIN4_Msk;
    GPIOB->DENSET = (GPIO_DENSET_PIN4_Msk | // CS
                     GPIO_DENSET_PIN5_Msk | // SCK
                     GPIO_DENSET_PIN6_Msk | // RX
                     GPIO_DENSET_PIN7_Msk); // TX

    EnableDelay();
}

void AT93C46_WriteEnable(void)
{
    SetCS();
    SPI->DR = AT93C46_OPCODE_WEN;
    while (SPI->SR_bit.BSY) {
    };
    ClrCS();
    (void)SPI->DR; //dummy read
    Delay(1);      //min 1us delay
}

void AT93C46_WriteDisable(void)
{
    SetCS();
    SPI->DR = AT93C46_OPCODE_WDS;
    while (SPI->SR_bit.BSY) {
    };
    ClrCS();
    (void)SPI->DR; //dummy read
    Delay(1);      //min 1us delay
}

uint32_t AT93C46_Write(uint16_t Addr, uint16_t Data)
{
    if (Addr < AT93C46_WORDS_TOTAL) {
        SetCS();
        SPI->DR = AT93C46_OPCODE_WRITE | Addr;
        SPI->DR = Data;
        while (SPI->SR_bit.BSY) {
        };
        ClrCS();
        (void)SPI->DR; //dummy read
        (void)SPI->DR; //dummy read
        Delay(15000);  //min 15ms delay
        return 0;
    } else
        return 1;
}

void AT93C46_WriteAll(uint16_t Data)
{
    SetCS();
    SPI->DR = AT93C46_OPCODE_WRALL;
    SPI->DR = Data;
    while (SPI->SR_bit.BSY) {
    };
    ClrCS();
    (void)SPI->DR; //dummy read
    (void)SPI->DR; //dummy read
    Delay(15000);  //min 15ms delay
}

uint16_t AT93C46_Read(uint16_t Addr)
{
    uint16_t buf = 0;
    if (Addr < AT93C46_WORDS_TOTAL) {
        SetCS();
        SPI->DR = AT93C46_OPCODE_READ | Addr;
        SPI->DR = 0x0000;
        SPI->DR = 0x0000;
        while (SPI->SR_bit.BSY) {
        };
        ClrCS();
        (void)SPI->DR;
        buf = SPI->DR;
        buf = (buf << 1) | (SPI->DR >> 15);
        Delay(1); //min 1us delay
    }
    return buf;
}

uint32_t AT93C46_Erase(uint16_t Addr)
{
    if (Addr < AT93C46_WORDS_TOTAL) {
        SetCS();
        SPI->DR = AT93C46_OPCODE_ERASE | Addr;
        while (SPI->SR_bit.BSY) {
        };
        ClrCS();
        (void)SPI->DR;
        Delay(15000); //min 15ms delay
        return 0;
    } else
        return 1;
}

void AT93C46_EraseAll(void)
{
    SetCS();
    SPI->DR = AT93C46_OPCODE_ERAL;
    while (SPI->SR_bit.BSY) {
    };
    ClrCS();
    (void)SPI->DR;
    Delay(15000); //min 15ms delay
}
