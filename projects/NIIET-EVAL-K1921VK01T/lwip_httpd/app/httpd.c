/*==============================================================================
 * Контроль веб-сервера
 *------------------------------------------------------------------------------
 * НИИЭТ, Дмитрий Сериков <lonie@niiet.ru>
 * НИИЭТ, Богдан Колбов <kolbov@niiet.ru>
 *==============================================================================
 * ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО
 * ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ ГАРАНТИИ ТОВАРНОЙ
 * ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ НАЗНАЧЕНИЮ И ОТСУТСТВИЯ
 * НАРУШЕНИЙ, НО НЕ ОГРАНИЧИВАЯСЬ ИМИ. ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ
 * ПРЕДНАЗНАЧЕНО ДЛЯ ОЗНАКОМИТЕЛЬНЫХ ЦЕЛЕЙ И НАПРАВЛЕНО ТОЛЬКО НА
 * ПРЕДОСТАВЛЕНИЕ ДОПОЛНИТЕЛЬНОЙ ИНФОРМАЦИИ О ПРОДУКТЕ, С ЦЕЛЬЮ СОХРАНИТЬ ВРЕМЯ
 * ПОТРЕБИТЕЛЮ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ
 * ОТВЕТСТВЕННОСТИ ПО КАКИМ-ЛИБО ИСКАМ, ЗА ПРЯМОЙ ИЛИ КОСВЕННЫЙ УЩЕРБ, ИЛИ
 * ПО ИНЫМ ТРЕБОВАНИЯМ, ВОЗНИКШИМ ИЗ-ЗА ИСПОЛЬЗОВАНИЯ ПРОГРАММНОГО ОБЕСПЕЧЕНИЯ
 * ИЛИ ИНЫХ ДЕЙСТВИЙ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.
 *
 *                              2019 АО "НИИЭТ"
 *==============================================================================
 */

//-- Includes ------------------------------------------------------------------
#include "httpd.h"
#include "fsdata.c"
#include "lwip/tcp.h"
#include <string.h>

//-- Private types -------------------------------------------------------------
struct http_state {
    char* file;
    u32_t left;
};

struct fs_file {
  char *data;
  int len;
};

//-- Private functions protoypes -----------------------------------------------
static int fs_open(char* name, struct fs_file* file);

//-- Private functions ---------------------------------------------------------
static void conn_err(void* arg, err_t err)
{
    struct http_state* hs;

    hs = arg;
    mem_free(hs);
}

static void close_conn(struct tcp_pcb* pcb, struct http_state* hs)
{

    tcp_arg(pcb, NULL);
    tcp_sent(pcb, NULL);
    tcp_recv(pcb, NULL);
    mem_free(hs);
    tcp_close(pcb);
}

static void send_data(struct tcp_pcb* pcb, struct http_state* hs)
{
    err_t err;
    u16_t len;

    /* We cannot send more data than space avaliable in the send
     buffer. */
    if (tcp_sndbuf(pcb) < hs->left) {
        len = tcp_sndbuf(pcb);
    } else {
        len = hs->left;
    }

    err = tcp_write(pcb, hs->file, len, 0);

    if (err == ERR_OK) {
        hs->file += len;
        hs->left -= len;
    }
}

static err_t http_poll(void* arg, struct tcp_pcb* pcb)
{
    if (arg == NULL) {
        //printf("Null, close\n");
        tcp_close(pcb);
    } else {
        send_data(pcb, (struct http_state*)arg);
    }

    return ERR_OK;
}

static err_t http_sent(void* arg, struct tcp_pcb* pcb, u16_t len)
{
    struct http_state* hs;

    hs = arg;

    if (hs->left > 0) {
        send_data(pcb, hs);
    } else {
        close_conn(pcb, hs);
    }

    return ERR_OK;
}

static err_t http_recv(void* arg, struct tcp_pcb* pcb, struct pbuf* p, err_t err)
{
    int i, j;
    char* data;
    char fname[40];
    struct fs_file file = { 0, 0 };
    struct http_state* hs;

    hs = arg;

    if (err == ERR_OK && p != NULL) {

        // Inform TCP that we have taken the data.
        tcp_recved(pcb, p->tot_len);

        if (hs->file == NULL) {
            data = p->payload;

            if (strncmp(data, "GET /method=get", 15) == 0) {
                i = 15;

                BSP_LED_Off(LED1_MSK | LED2_MSK | LED3_MSK |
                            LED4_MSK | LED5_MSK | LED6_MSK | LED7_MSK);
                while (data[i] != ' ') {
                    i++;
                    if (data[i] == 'l') {
                        i++;
                        if (data[i] == 'e') {
                            i++;
                            if (data[i] == 'd') {
                                i += 2;
                                if (data[i] == '1') {
                                    BSP_LED_On(LED1_MSK);
                                }
                                if (data[i] == '2') {
                                    BSP_LED_On(LED2_MSK);
                                }
                                if (data[i] == '3') {
                                    BSP_LED_On(LED3_MSK);
                                }
                                if (data[i] == '4') {
                                    BSP_LED_On(LED4_MSK);
                                }
                                if (data[i] == '5') {
                                    BSP_LED_On(LED5_MSK);
                                }
                                if (data[i] == '6') {
                                    BSP_LED_On(LED6_MSK);
                                }
                                if (data[i] == '7') {
                                    BSP_LED_On(LED7_MSK);
                                }
                            }
                        }
                    }
                }

                pbuf_free(p);
                fs_open("/index.html", &file);
                hs->file = file.data;
                hs->left = file.len;
                send_data(pcb, hs);

                // Tell TCP that we wish be to informed of data that has been
                // successfully sent by a call to the http_sent() function.
                tcp_sent(pcb, http_sent);
            } else if (strncmp(data, "GET ", 4) == 0) {
                for (i = 0; i < 40; i++) {
                    if (((char*)data + 4)[i] == ' ' ||
                        ((char*)data + 4)[i] == '\r' ||
                        ((char*)data + 4)[i] == '\n') {
                        ((char*)data + 4)[i] = 0;
                    }
                }

                i = 0;
                j = 0;

                do {
                    fname[i] = ((char*)data + 4)[j];
                    j++;
                    i++;
                } while (fname[i - 1] != 0 && i < 40);

                pbuf_free(p);

                if (strcmp(fname, "/") == 0) {
                    fs_open("/index.html", &file);
                } else if (!fs_open(fname, &file)) {
                    fs_open("/404.html", &file);
                }

                hs->file = file.data;
                hs->left = file.len;
                send_data(pcb, hs);

                // Tell TCP that we wish be to informed of data that has been
                // successfully sent by a call to the http_sent() function.
                tcp_sent(pcb, http_sent);
            } else {
                close_conn(pcb, hs);
            }
        } else {
            pbuf_free(p);
        }
    }

    if (err == ERR_OK && p == NULL) {

        close_conn(pcb, hs);
    }

    return ERR_OK;
}

static err_t http_accept(void* arg, struct tcp_pcb* pcb, err_t err)
{
    struct http_state* hs;

    // Allocate memory for the structure that holds the state of the connection.
    hs = mem_malloc(sizeof(struct http_state));

    if (hs == NULL) {
        return ERR_MEM;
    }

    // Initialize the structure.
    hs->file = NULL;
    hs->left = 0;

    // Tell TCP that this is the structure we wish to be passed for our callbacks.
    tcp_arg(pcb, hs);

    // Tell TCP that we wish to be informed of incoming data by a call
    // to the http_recv() function.
    tcp_recv(pcb, http_recv);

    tcp_err(pcb, conn_err);

    tcp_poll(pcb, http_poll, 10);
    return ERR_OK;
}

void httpd_init(void)
{
    struct tcp_pcb* pcb;

    pcb = tcp_new();
    tcp_bind(pcb, IP_ADDR_ANY, 80);
    pcb = tcp_listen(pcb);
    tcp_accept(pcb, http_accept);
}

static int fs_open(char* name, struct fs_file* file)
{
    struct fsdata_file_noconst* f;

    for (f = (struct fsdata_file_noconst*)FS_ROOT; f != NULL; f = (struct fsdata_file_noconst*)f->next) {
        if (!strcmp(name, f->name)) {
            file->data = f->data;
            file->len = f->len;
            return 1;
        }
    }
    return 0;
}
