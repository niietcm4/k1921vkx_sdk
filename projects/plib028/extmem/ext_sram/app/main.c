/*==============================================================================
 * Пример чтения и записи во внешнею ОЗУ размером 128кБ
 *------------------------------------------------------------------------------
 * НИИЭТ, Богдан Колбов <kolbov@niiet.ru>
 *==============================================================================
 * ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО
 * ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ ГАРАНТИИ ТОВАРНОЙ
 * ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ НАЗНАЧЕНИЮ И ОТСУТСТВИЯ
 * НАРУШЕНИЙ, НО НЕ ОГРАНИЧИВАЯСЬ ИМИ. ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ
 * ПРЕДНАЗНАЧЕНО ДЛЯ ОЗНАКОМИТЕЛЬНЫХ ЦЕЛЕЙ И НАПРАВЛЕНО ТОЛЬКО НА
 * ПРЕДОСТАВЛЕНИЕ ДОПОЛНИТЕЛЬНОЙ ИНФОРМАЦИИ О ПРОДУКТЕ, С ЦЕЛЬЮ СОХРАНИТЬ ВРЕМЯ
 * ПОТРЕБИТЕЛЮ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ
 * ОТВЕТСТВЕННОСТИ ПО КАКИМ-ЛИБО ИСКАМ, ЗА ПРЯМОЙ ИЛИ КОСВЕННЫЙ УЩЕРБ, ИЛИ
 * ПО ИНЫМ ТРЕБОВАНИЯМ, ВОЗНИКШИМ ИЗ-ЗА ИСПОЛЬЗОВАНИЯ ПРОГРАММНОГО ОБЕСПЕЧЕНИЯ
 * ИЛИ ИНЫХ ДЕЙСТВИЙ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.
 *
 *                              2019 АО "НИИЭТ"
 *==============================================================================
 */

//-- Includes ------------------------------------------------------------------
#include "plib028.h"
#include "retarget_conf.h"

//-- Defines -------------------------------------------------------------------
#define DATA_WORDS 8
#define SRAM_WINDOW EXTMEM_WindowNum_2

//-- Peripheral init functions -------------------------------------------------
void gpio_init()
{
    RCU_AHBClkCmd(RCU_AHBClk_GPIOA, ENABLE);
    RCU_AHBRstCmd(RCU_AHBRst_GPIOA, ENABLE);
    GPIO_OutCmd(GPIOA, GPIO_Pin_7_0, ENABLE);
    GPIO_DigitalCmd(GPIOA, GPIO_Pin_7_0, ENABLE);
    GPIO_SetBits(GPIOA, GPIO_Pin_7_0);
}

void extmem_init()
{
    EXTMEM_Init_TypeDef EXTMEM_InitStruct;
    GPIO_Init_TypeDef GPIO_InitStruct;

    RCU_AHBClkCmd(RCU_AHBClk_EXTMEM, ENABLE);
    RCU_AHBRstCmd(RCU_AHBRst_EXTMEM, ENABLE);

    EXTMEM_StructInit(&EXTMEM_InitStruct);
    EXTMEM_InitStruct.Mode = EXTMEM_Mode_16bit;
    EXTMEM_InitStruct.RDCycle = 15;
    EXTMEM_InitStruct.WRCycle = 15;
    EXTMEM_InitStruct.TACycle = 15;
    EXTMEM_Init(SRAM_WINDOW, &EXTMEM_InitStruct);

    RCU_AHBClkCmd(RCU_AHBClk_GPIOD, ENABLE);
    RCU_AHBClkCmd(RCU_AHBClk_GPIOE, ENABLE);
    RCU_AHBClkCmd(RCU_AHBClk_GPIOF, ENABLE);
    RCU_AHBClkCmd(RCU_AHBClk_GPIOG, ENABLE);
    RCU_AHBRstCmd(RCU_AHBRst_GPIOD, ENABLE);
    RCU_AHBRstCmd(RCU_AHBRst_GPIOE, ENABLE);
    RCU_AHBRstCmd(RCU_AHBRst_GPIOF, ENABLE);
    RCU_AHBRstCmd(RCU_AHBRst_GPIOG, ENABLE);

    // D6 - CE
    // D12 - LB
    // D13 - WE
    // D14 - UB
    // D15 - OE
    GPIO_StructInit(&GPIO_InitStruct);
    GPIO_InitStruct.AltFuncNum = GPIO_AltFuncNum_1;
    GPIO_InitStruct.AltFunc = ENABLE;
    GPIO_InitStruct.Pin = GPIO_Pin_6 | GPIO_Pin_12_15;
    GPIO_Init(GPIOD, &GPIO_InitStruct);
    GPIO_DigitalCmd(GPIOD, GPIO_InitStruct.Pin, ENABLE);

    // GPIOE[15:0] - DATA
    GPIO_InitStruct.Pin = GPIO_Pin_All;
    GPIO_Init(GPIOE, &GPIO_InitStruct);
    GPIO_DigitalCmd(GPIOE, GPIO_InitStruct.Pin, ENABLE);

    // GPIOF[15:1] - ADDR[15:1]
    GPIO_InitStruct.Pin = GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3 | GPIO_Pin_4_7 | GPIO_Pin_8_15;
    GPIO_Init(GPIOF, &GPIO_InitStruct);
    GPIO_DigitalCmd(GPIOF, GPIO_InitStruct.Pin, ENABLE);

    // G16 - ADDR[16]
    GPIO_InitStruct.Pin = GPIO_Pin_0;
    GPIO_Init(GPIOG, &GPIO_InitStruct);
    GPIO_DigitalCmd(GPIOG, GPIO_InitStruct.Pin, ENABLE);
}

void periph_init()
{
    SystemCoreClockUpdate();
    retarget_init();
    gpio_init();
    extmem_init();
    printf("\nAll peripherals inited, SYSCLK = %3d MHz\n", (int)(SystemCoreClock / 1E6));
}

//-- Main ----------------------------------------------------------------------
int main()
{
    uint32_t data_wr[DATA_WORDS] = { 0x80818283,
                                     0x94959697,
                                     0xA8A9AAAB,
                                     0xBCBDBEBF,
                                     0xC0C1C2C3,
                                     0xD4D5D6D7,
                                     0xE8E9EAEB,
                                     0x12345678 };
    uint32_t data_rd[DATA_WORDS];
    int addr_offset = 0x4;

    periph_init();
    SysTick_Config(SystemCoreClock / 1000 - 1); //1ms tick

    printf("Write to external SRAM:\n");
    for (int i = 0; i < DATA_WORDS; i++) {
        EXTMEM_Write32(SRAM_WINDOW, i * addr_offset, data_wr[i]);
        printf("%d: 0x%05x <- 0x%08x\n", i, i * addr_offset, (int)data_wr[i]);
    }
    printf("Read from external SRAM:\n");
    for (int i = 0; i < DATA_WORDS; i++) {
        data_rd[i] = EXTMEM_Read32(SRAM_WINDOW, i * addr_offset);
        printf("%d: 0x%05x -> 0x%08x\n", i, i * addr_offset, (int)data_rd[i]);
    }
    printf("Check results:\n");
    for (int i = 0; i < DATA_WORDS; i++) {
        if (data_rd[i] != data_wr[i])
            printf("%d: addr 0x%05x error, expected 0x%08x, got 0x%08x\n", i, i * addr_offset, (int)data_wr[i], (int)data_rd[i]);
        else
            printf("%d: addr 0x%05x ok\n", i, i * addr_offset);
    }

    while (1) {
    };
    return 0;
}

//-- IRQ handlers --------------------------------------------------------------
volatile uint32_t tick_counter = 0;
void SysTick_Handler()
{
    if (tick_counter >= 200) {
        tick_counter = 0;
        GPIO_ToggleBits(GPIOA, GPIO_Pin_0);
    } else
        tick_counter++;
}

//-- Assert --------------------------------------------------------------------
#if defined USE_FULL_ASSERT
void assert_failed(uint8_t* file, uint32_t line)
{
    printf("Assert failed: file %s on line %d\n", file, (int)line);
    while (1) {
    };
}
#endif /* USE_FULL_ASSERT */
