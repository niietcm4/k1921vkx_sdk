/*==============================================================================
 * Пример включения каналов выского разрешения ШИМ. HR(high-resolution) задержка
 * изменяется каждый период ШИМ от минимального до максимального значения.
 *
 * PWM0 - hr (high-resolution) задержка переднего фронта канала A относительно B
 * PWM1 - hr задержка заднего фронта канала A относительно B
 * PWM2 - hr задержка обоих фронтов канала A относительно B
 * PWM3 - hr задержка переднего фронта канала B относительно A
 * PWM4 - hr задержка заднего фронта канала B относительно A
 * PWM5 - hr задержка обоих фронтов канала B относительно A
 *
 *------------------------------------------------------------------------------
 * НИИЭТ, Роман Степаненко <hgost@niiet.ru>
 * НИИЭТ, Богдан Колбов <kolbov@niiet.ru>
 *==============================================================================
 * ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО
 * ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ ГАРАНТИИ ТОВАРНОЙ
 * ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ НАЗНАЧЕНИЮ И ОТСУТСТВИЯ
 * НАРУШЕНИЙ, НО НЕ ОГРАНИЧИВАЯСЬ ИМИ. ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ
 * ПРЕДНАЗНАЧЕНО ДЛЯ ОЗНАКОМИТЕЛЬНЫХ ЦЕЛЕЙ И НАПРАВЛЕНО ТОЛЬКО НА
 * ПРЕДОСТАВЛЕНИЕ ДОПОЛНИТЕЛЬНОЙ ИНФОРМАЦИИ О ПРОДУКТЕ, С ЦЕЛЬЮ СОХРАНИТЬ ВРЕМЯ
 * ПОТРЕБИТЕЛЮ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ
 * ОТВЕТСТВЕННОСТИ ПО КАКИМ-ЛИБО ИСКАМ, ЗА ПРЯМОЙ ИЛИ КОСВЕННЫЙ УЩЕРБ, ИЛИ
 * ПО ИНЫМ ТРЕБОВАНИЯМ, ВОЗНИКШИМ ИЗ-ЗА ИСПОЛЬЗОВАНИЯ ПРОГРАММНОГО ОБЕСПЕЧЕНИЯ
 * ИЛИ ИНЫХ ДЕЙСТВИЙ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.
 *
 *                              2018 АО "НИИЭТ"
 *==============================================================================
 */

//-- Includes ------------------------------------------------------------------
#include "K1921VK028.h"
#include "bsp.h"
#include "retarget_conf.h"

//-- Defines -------------------------------------------------------------------

//-- Peripheral init functions -------------------------------------------------
void pwm_init()
{
    RCU->HCLKCFG_bit.GPIOLEN = 1;
    RCU->HRSTCFG_bit.GPIOLEN = 1;
    RCU->PCLKCFG1 = RCU_PCLKCFG1_PWM0EN_Msk | RCU_PCLKCFG1_PWM1EN_Msk |
                    RCU_PCLKCFG1_PWM2EN_Msk | RCU_PCLKCFG1_PWM3EN_Msk |
                    RCU_PCLKCFG1_PWM4EN_Msk | RCU_PCLKCFG1_PWM5EN_Msk;
    RCU->PRSTCFG1 = RCU_PRSTCFG1_PWM0EN_Msk | RCU_PRSTCFG1_PWM1EN_Msk |
                    RCU_PRSTCFG1_PWM2EN_Msk | RCU_PRSTCFG1_PWM3EN_Msk |
                    RCU_PRSTCFG1_PWM4EN_Msk | RCU_PRSTCFG1_PWM5EN_Msk;

    GPIOL->DENSET = 0xFFF;
    GPIOL->ALTFUNCSET = 0xFFF;
    GPIOL->ALTFUNCNUM0 = 0x22222222;
    GPIOL->ALTFUNCNUM1 = (GPIOL->ALTFUNCNUM1 & (~0xFFFF)) | 0x2222;

    PWM_TypeDef* PWM;

    for (int i = 0; i < 6; i++) {
        PWM = (PWM_TypeDef*)(PWM0_BASE + i * 0x1000);

        PWM->TBPRD = 0x100;
        PWM->CMPA_bit.CMPA = 0x80;

        PWM->AQCTLA_bit.CAU = PWM_AQCTLA_CAD_Set;
        PWM->AQCTLA_bit.CAD = PWM_AQCTLA_CAD_Clear;

        PWM->AQCTLB_bit.CAU = PWM_AQCTLB_CAD_Set;
        PWM->AQCTLB_bit.CAD = PWM_AQCTLB_CAD_Clear;

        PWM->TBCTL_bit.CTRMODE = PWM_TBCTL_CTRMODE_UpDown;
    }

    PWM0->HRCTL_bit.EDGMODEA = PWM_HRCTL_EDGMODEA_PosEdge;
    PWM1->HRCTL_bit.EDGMODEA = PWM_HRCTL_EDGMODEA_NegEdge;
    PWM2->HRCTL_bit.EDGMODEA = PWM_HRCTL_EDGMODEA_BothEdge;
    PWM0->HRCTL_bit.CTLMODEA = PWM_HRCTL_CTLMODEA_CMPAHR;
    PWM1->HRCTL_bit.CTLMODEA = PWM_HRCTL_CTLMODEA_CMPAHR;
    PWM2->HRCTL_bit.CTLMODEA = PWM_HRCTL_CTLMODEA_CMPAHR;

    PWM3->HRCTL_bit.EDGMODEB = PWM_HRCTL_EDGMODEB_PosEdge;
    PWM4->HRCTL_bit.EDGMODEB = PWM_HRCTL_EDGMODEB_NegEdge;
    PWM5->HRCTL_bit.EDGMODEB = PWM_HRCTL_EDGMODEB_BothEdge;
    PWM3->HRCTL_bit.CTLMODEB = PWM_HRCTL_CTLMODEB_TBPHSHR;
    PWM4->HRCTL_bit.CTLMODEB = PWM_HRCTL_CTLMODEB_TBPHSHR;
    PWM5->HRCTL_bit.CTLMODEB = PWM_HRCTL_CTLMODEB_TBPHSHR;

    PWM0->ETSEL = PWM_ETSEL_INTSEL_CTREqPRD << PWM_ETSEL_INTSEL_Pos |
                  PWM_ETSEL_INTEN_Msk;
    NVIC_EnableIRQ(PWM0_IRQn);
}

void periph_init()
{
    SystemCoreClockUpdate();
    retarget_init();
    BSP_LED_Init();
    pwm_init();
    printf("\nAll peripherals inited, SYSCLK = %3d MHz\n", (int)(SystemCoreClock / 1E6));
}

//-- Main ----------------------------------------------------------------------
int main()
{
    periph_init();
    SysTick_Config(10000000);

    SIU->PWMSYNC_bit.PRESCRST = 0x3F; // pwm count en

    while (1) {
    };
    return 0;
}

//-- IRQ handlers --------------------------------------------------------------
void SysTick_Handler()
{
    BSP_LED_Toggle(LED0_MSK);
}

volatile uint32_t hr_cycle = 0;
void PWM0_IRQHandler()
{
    PWM3->TBPHS_bit.TBPHSHR = (hr_cycle);
    PWM4->TBPHS_bit.TBPHSHR = (hr_cycle);
    PWM5->TBPHS_bit.TBPHSHR = (hr_cycle);

    PWM0->CMPA_bit.CMPAHR = (255 - hr_cycle);
    PWM1->CMPA_bit.CMPAHR = (255 - hr_cycle);
    PWM2->CMPA_bit.CMPAHR = (255 - hr_cycle);

    PWM0->INTCLR = PWM_INTCLR_INT_Msk;
    PWM0->ETCLR = PWM_ETCLR_INT_Msk;
    if (hr_cycle < 255)
        hr_cycle++;
    else
        hr_cycle = 1;
}

void PWM1_IRQHandler()
{
    static uint32_t hd_cntr=0;
    static uint32_t ctr0=0;
    static uint32_t ctr1=0;
    if (ctr0==0 & PWM1->HRCTL_bit.DELAYCALA == 1)
        ctr0=hd_cntr;
    if (ctr1==0 & PWM1->HRCTL_bit.DELAYCALA == 3)
        ctr1=hd_cntr;
    if (hd_cntr<255 & ctr1==0)
        hd_cntr++;
    else
    {
        TB_INFO=ctr0;
        TB_INFO=ctr1;
        IRQ_DIS(PWM1_IRQn);
    }
    PWM1->CMPA_bit.CMPAHR=hd_cntr;
    PWM1->INTCLR = 0x00000001;
    PWM1->ETCLR = 0x00000001;
}