/*==============================================================================
 * Управление периферией платы NIIET-BB-K1921VK035
 *------------------------------------------------------------------------------
 * НИИЭТ, Богдан Колбов <kolbov@niiet.ru>
 *==============================================================================
 * ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО
 * ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ ГАРАНТИИ ТОВАРНОЙ
 * ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ НАЗНАЧЕНИЮ И ОТСУТСТВИЯ
 * НАРУШЕНИЙ, НО НЕ ОГРАНИЧИВАЯСЬ ИМИ. ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ
 * ПРЕДНАЗНАЧЕНО ДЛЯ ОЗНАКОМИТЕЛЬНЫХ ЦЕЛЕЙ И НАПРАВЛЕНО ТОЛЬКО НА
 * ПРЕДОСТАВЛЕНИЕ ДОПОЛНИТЕЛЬНОЙ ИНФОРМАЦИИ О ПРОДУКТЕ, С ЦЕЛЬЮ СОХРАНИТЬ ВРЕМЯ
 * ПОТРЕБИТЕЛЮ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ
 * ОТВЕТСТВЕННОСТИ ПО КАКИМ-ЛИБО ИСКАМ, ЗА ПРЯМОЙ ИЛИ КОСВЕННЫЙ УЩЕРБ, ИЛИ
 * ПО ИНЫМ ТРЕБОВАНИЯМ, ВОЗНИКШИМ ИЗ-ЗА ИСПОЛЬЗОВАНИЯ ПРОГРАММНОГО ОБЕСПЕЧЕНИЯ
 * ИЛИ ИНЫХ ДЕЙСТВИЙ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.
 *
 *                              2018 АО "НИИЭТ"
 *==============================================================================
 */

//-- Includes ------------------------------------------------------------------
#include "bsp.h"

//-- Private variables ---------------------------------------------------------
static volatile uint32_t btn_press_event = 0;

//-- Functions -----------------------------------------------------------------
void BSP_LED_Init()
{
    RCU->HCLKCFG_bit.LED_PORT_EN = 1;
    RCU->HRSTCFG_bit.LED_PORT_EN = 1;
    LED_PORT->DENSET = LED_PIN_MSK;
    LED_PORT->OUTENSET = LED_PIN_MSK;
}

void BSP_LED_Toggle()
{
    LED_PORT->DATAOUTTGL = LED_PIN_MSK;
}

void BSP_LED_On()
{
    LED_PORT->DATAOUTSET = LED_PIN_MSK;
}

void BSP_LED_Off()
{
    LED_PORT->DATAOUTCLR = LED_PIN_MSK;
}

void BSP_Btn_Init()
{
    // Настройка выводов
    RCU->HCLKCFG_bit.BTN_PORT_EN = 1;
    RCU->HRSTCFG_bit.BTN_PORT_EN = 1;
    BTN_PORT->DENSET = BTN_PIN_MSK;
    BTN_PORT->INTTYPESET = (1 << BTN_PIN_POS); // фронт
    BTN_PORT->INTPOLSET = (1 << BTN_PIN_POS);  // положительный
    BTN_PORT->INTENSET = BTN_PIN_MSK;
    NVIC_EnableIRQ(BTN_IRQ_N);
}

uint32_t BSP_Btn_IsPressed()
{
    if (btn_press_event) {
        btn_press_event = 0;
        return 1;
    } else
        return 0;
}

//-- IRQ handlers --------------------------------------------------------------
void BTN_IRQ_HANDLER()
{
    BTN_PORT->INTSTATUS = BTN_PIN_MSK;
    btn_press_event = 1;
}
