 ======================================================================================
|                                 Fin =  16 000 000                                    |
|======================================================================================|
|             |             |             |             |     |     |    |     |       |
|     Fout    |   PLL_Fout  |   PLL_Fvco  |   PLL_Fref  |  N  |  M  | OD | DIV | DIVEN |
|=============|=============|=============|=============|=====|=====|====|=====|=======|
|   1 000 000 |  16 000 000 | 128 000 000 |   4 000 000 |  4  |  32 |  3 |   7 |   1   |
|   2 000 000 |  16 000 000 | 128 000 000 |   4 000 000 |  4  |  32 |  3 |   3 |   1   |
|   3 000 000 |  18 000 000 | 144 000 000 |   4 000 000 |  4  |  36 |  3 |   2 |   1   |
|   4 000 000 |  16 000 000 | 128 000 000 |   4 000 000 |  4  |  32 |  3 |   1 |   1   |
|   5 000 000 |  20 000 000 | 160 000 000 |   4 000 000 |  4  |  40 |  3 |   1 |   1   |
|   6 000 000 |  24 000 000 | 192 000 000 |   4 000 000 |  4  |  48 |  3 |   1 |   1   |
|   7 000 000 |  42 000 000 | 168 000 000 |   4 000 000 |  4  |  42 |  2 |   2 |   1   |
|   8 000 000 |  16 000 000 | 128 000 000 |   4 000 000 |  4  |  32 |  3 |   0 |   1   |
|   9 000 000 |  18 000 000 | 144 000 000 |   4 000 000 |  4  |  36 |  3 |   0 |   1   |
|  10 000 000 |  20 000 000 | 160 000 000 |   4 000 000 |  4  |  40 |  3 |   0 |   1   |
|  11 000 000 |  22 000 000 | 176 000 000 |   4 000 000 |  4  |  44 |  3 |   0 |   1   |
|  12 000 000 |  24 000 000 | 192 000 000 |   4 000 000 |  4  |  48 |  3 |   0 |   1   |
|  13 000 000 |  78 000 000 | 156 000 000 |   4 000 000 |  4  |  39 |  1 |   2 |   1   |
|  14 000 000 |  84 000 000 | 168 000 000 |   4 000 000 |  4  |  42 |  1 |   2 |   1   |
|  15 000 000 |  15 000 000 | 120 000 000 |   4 000 000 |  4  |  30 |  3 |   0 |   0   |
|  16 000 000 |  16 000 000 | 128 000 000 |   4 000 000 |  4  |  32 |  3 |   0 |   0   |
|  17 000 000 |  17 000 000 | 136 000 000 |   4 000 000 |  4  |  34 |  3 |   0 |   0   |
|  18 000 000 |  18 000 000 | 144 000 000 |   4 000 000 |  4  |  36 |  3 |   0 |   0   |
|  19 000 000 |  19 000 000 | 152 000 000 |   4 000 000 |  4  |  38 |  3 |   0 |   0   |
|  20 000 000 |  20 000 000 | 160 000 000 |   4 000 000 |  4  |  40 |  3 |   0 |   0   |
|  21 000 000 |  21 000 000 | 168 000 000 |   4 000 000 |  4  |  42 |  3 |   0 |   0   |
|  22 000 000 |  22 000 000 | 176 000 000 |   4 000 000 |  4  |  44 |  3 |   0 |   0   |
|  23 000 000 |  23 000 000 | 184 000 000 |   4 000 000 |  4  |  46 |  3 |   0 |   0   |
|  24 000 000 |  24 000 000 | 192 000 000 |   4 000 000 |  4  |  48 |  3 |   0 |   0   |
|  25 000 000 |  25 000 000 | 200 000 000 |   4 000 000 |  4  |  50 |  3 |   0 |   0   |
|  26 000 000 | 156 000 000 | 156 000 000 |   4 000 000 |  4  |  39 |  0 |   2 |   1   |
|  28 000 000 | 168 000 000 | 168 000 000 |   4 000 000 |  4  |  42 |  0 |   2 |   1   |
|  30 000 000 |  30 000 000 | 120 000 000 |   4 000 000 |  4  |  30 |  2 |   0 |   0   |
|  31 000 000 |  31 000 000 | 124 000 000 |   4 000 000 |  4  |  31 |  2 |   0 |   0   |
|  32 000 000 |  32 000 000 | 128 000 000 |   4 000 000 |  4  |  32 |  2 |   0 |   0   |
|  33 000 000 |  33 000 000 | 132 000 000 |   4 000 000 |  4  |  33 |  2 |   0 |   0   |
|  34 000 000 |  34 000 000 | 136 000 000 |   4 000 000 |  4  |  34 |  2 |   0 |   0   |
|  35 000 000 |  35 000 000 | 140 000 000 |   4 000 000 |  4  |  35 |  2 |   0 |   0   |
|  36 000 000 |  36 000 000 | 144 000 000 |   4 000 000 |  4  |  36 |  2 |   0 |   0   |
|  37 000 000 |  37 000 000 | 148 000 000 |   4 000 000 |  4  |  37 |  2 |   0 |   0   |
|  38 000 000 |  38 000 000 | 152 000 000 |   4 000 000 |  4  |  38 |  2 |   0 |   0   |
|  39 000 000 |  39 000 000 | 156 000 000 |   4 000 000 |  4  |  39 |  2 |   0 |   0   |
|  40 000 000 |  40 000 000 | 160 000 000 |   4 000 000 |  4  |  40 |  2 |   0 |   0   |
|  41 000 000 |  41 000 000 | 164 000 000 |   4 000 000 |  4  |  41 |  2 |   0 |   0   |
|  42 000 000 |  42 000 000 | 168 000 000 |   4 000 000 |  4  |  42 |  2 |   0 |   0   |
|  43 000 000 |  43 000 000 | 172 000 000 |   4 000 000 |  4  |  43 |  2 |   0 |   0   |
|  44 000 000 |  44 000 000 | 176 000 000 |   4 000 000 |  4  |  44 |  2 |   0 |   0   |
|  45 000 000 |  45 000 000 | 180 000 000 |   4 000 000 |  4  |  45 |  2 |   0 |   0   |
|  46 000 000 |  46 000 000 | 184 000 000 |   4 000 000 |  4  |  46 |  2 |   0 |   0   |
|  47 000 000 |  47 000 000 | 188 000 000 |   4 000 000 |  4  |  47 |  2 |   0 |   0   |
|  48 000 000 |  48 000 000 | 192 000 000 |   4 000 000 |  4  |  48 |  2 |   0 |   0   |
|  49 000 000 |  49 000 000 | 196 000 000 |   4 000 000 |  4  |  49 |  2 |   0 |   0   |
|  50 000 000 |  50 000 000 | 200 000 000 |   4 000 000 |  4  |  50 |  2 |   0 |   0   |
|  60 000 000 |  60 000 000 | 120 000 000 |   4 000 000 |  4  |  30 |  1 |   0 |   0   |
|  62 000 000 |  62 000 000 | 124 000 000 |   4 000 000 |  4  |  31 |  1 |   0 |   0   |
|  64 000 000 |  64 000 000 | 128 000 000 |   4 000 000 |  4  |  32 |  1 |   0 |   0   |
|  66 000 000 |  66 000 000 | 132 000 000 |   4 000 000 |  4  |  33 |  1 |   0 |   0   |
|  68 000 000 |  68 000 000 | 136 000 000 |   4 000 000 |  4  |  34 |  1 |   0 |   0   |
|  70 000 000 |  70 000 000 | 140 000 000 |   4 000 000 |  4  |  35 |  1 |   0 |   0   |
|  72 000 000 |  72 000 000 | 144 000 000 |   4 000 000 |  4  |  36 |  1 |   0 |   0   |
|  74 000 000 |  74 000 000 | 148 000 000 |   4 000 000 |  4  |  37 |  1 |   0 |   0   |
|  76 000 000 |  76 000 000 | 152 000 000 |   4 000 000 |  4  |  38 |  1 |   0 |   0   |
|  78 000 000 |  78 000 000 | 156 000 000 |   4 000 000 |  4  |  39 |  1 |   0 |   0   |
|  80 000 000 |  80 000 000 | 160 000 000 |   4 000 000 |  4  |  40 |  1 |   0 |   0   |
|  82 000 000 |  82 000 000 | 164 000 000 |   4 000 000 |  4  |  41 |  1 |   0 |   0   |
|  84 000 000 |  84 000 000 | 168 000 000 |   4 000 000 |  4  |  42 |  1 |   0 |   0   |
|  86 000 000 |  86 000 000 | 172 000 000 |   4 000 000 |  4  |  43 |  1 |   0 |   0   |
|  88 000 000 |  88 000 000 | 176 000 000 |   4 000 000 |  4  |  44 |  1 |   0 |   0   |
|  90 000 000 |  90 000 000 | 180 000 000 |   4 000 000 |  4  |  45 |  1 |   0 |   0   |
|  92 000 000 |  92 000 000 | 184 000 000 |   4 000 000 |  4  |  46 |  1 |   0 |   0   |
|  94 000 000 |  94 000 000 | 188 000 000 |   4 000 000 |  4  |  47 |  1 |   0 |   0   |
|  96 000 000 |  96 000 000 | 192 000 000 |   4 000 000 |  4  |  48 |  1 |   0 |   0   |
|  98 000 000 |  98 000 000 | 196 000 000 |   4 000 000 |  4  |  49 |  1 |   0 |   0   |
| 100 000 000 | 100 000 000 | 200 000 000 |   4 000 000 |  4  |  50 |  1 |   0 |   0   |
