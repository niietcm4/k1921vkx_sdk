/*==============================================================================
 * Реализация класса HID USB Device - геймпад из 5-ти кнопок.
 * Однако, на плате доступны только две кнопки - остальные будут оставаться
 * неактивными.
 *------------------------------------------------------------------------------
 * НИИЭТ, Дмитрий Сериков <lonie@niiet.ru>
 * НИИЭТ, Богдан Колбов <kolbov@niiet.ru>
 *==============================================================================
 * ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО
 * ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ ГАРАНТИИ ТОВАРНОЙ
 * ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ НАЗНАЧЕНИЮ И ОТСУТСТВИЯ
 * НАРУШЕНИЙ, НО НЕ ОГРАНИЧИВАЯСЬ ИМИ. ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ
 * ПРЕДНАЗНАЧЕНО ДЛЯ ОЗНАКОМИТЕЛЬНЫХ ЦЕЛЕЙ И НАПРАВЛЕНО ТОЛЬКО НА
 * ПРЕДОСТАВЛЕНИЕ ДОПОЛНИТЕЛЬНОЙ ИНФОРМАЦИИ О ПРОДУКТЕ, С ЦЕЛЬЮ СОХРАНИТЬ ВРЕМЯ
 * ПОТРЕБИТЕЛЮ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ
 * ОТВЕТСТВЕННОСТИ ПО КАКИМ-ЛИБО ИСКАМ, ЗА ПРЯМОЙ ИЛИ КОСВЕННЫЙ УЩЕРБ, ИЛИ
 * ПО ИНЫМ ТРЕБОВАНИЯМ, ВОЗНИКШИМ ИЗ-ЗА ИСПОЛЬЗОВАНИЯ ПРОГРАММНОГО ОБЕСПЕЧЕНИЯ
 * ИЛИ ИНЫХ ДЕЙСТВИЙ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.
 *
 *                              2019 АО "НИИЭТ"
 *==============================================================================
 */

//-- Includes ------------------------------------------------------------------
#include "K1921VK01T.h"
#include "bsp.h"
#include "retarget_conf.h"
#include "system_K1921VK01T.h"
#include "usb_hid_gamepad.h"
#include "usb_otg.h"

//-- Defines -------------------------------------------------------------------

//-- Functions prototypes ------------------------------------------------------

//-- Variables -----------------------------------------------------------------

//-- Peripheral init functions -------------------------------------------------
void usb_init()
{
    BSP_USB_Init();
    USBDev_Init();
    USBDev_ClassCbInit(&USBDEV_HID_cb);
    USBOTG_SetRole(USB_Role_Device);

    NVIC_EnableIRQ(USBOTG_IRQn);
}

void periph_init()
{
    SystemCoreClockUpdate();
    retarget_init();
    BSP_LED_Init();
    BSP_Btn_Init();
    usb_init();
    printf("\nAll peripherals inited, SYSCLK = %3d MHz\n", (int)(SystemCoreClock / 1E6));
}

//-- Main ----------------------------------------------------------------------

int main(void)
{
    periph_init();
    SysTick_Config(SystemCoreClock / 1000 - 1); // 1ms tick

    while (1) {
    };

    return 0;
}

//-- IRQ handlers --------------------------------------------------------------
volatile uint32_t tick_counter = 0;


void SysTick_Handler(void)
{
    BSP_Btn_State_TypeDef btn_state;
    __ALIGNED(4) hid_report_t hid_report;
    if (tick_counter >= 10) //10 ms polling
    {
        tick_counter = 0;
        btn_state = BSP_Btn_GetState();
        hid_report.buttons.byteval = 0;
        switch (btn_state) {
        case (BSP_Btn_State_SB2):
            hid_report.buttons.bitval.b0 = 1;
            hid_report.buttons.bitval.b1 = 0;
            BSP_LED_On(LED0_MSK);
            BSP_LED_Off(LED1_MSK);
            break;
        case (BSP_Btn_State_SB3):
            hid_report.buttons.bitval.b0 = 0;
            hid_report.buttons.bitval.b1 = 1;
            BSP_LED_Off(LED0_MSK);
            BSP_LED_On(LED1_MSK);
            break;
        case (BSP_Btn_State_Both):
            hid_report.buttons.bitval.b0 = 1;
            hid_report.buttons.bitval.b1 = 1;
            BSP_LED_On(LED0_MSK);
            BSP_LED_On(LED1_MSK);
            break;
        default:
            hid_report.buttons.bitval.b0 = 0;
            hid_report.buttons.bitval.b1 = 0;
            BSP_LED_Off(LED0_MSK);
            BSP_LED_Off(LED1_MSK);
            break;
        }
        HID_SendReport(&hid_report);
    } else
        tick_counter++;
}

void USBOTG_IRQHandler(void)
{
    USBOTG_IntHandler();
}
