/*==============================================================================
 * Реализация обработки вращения инкрементального энкодера.
 * Счетчик позиции настраивается так, чтобы один щелчок энкодера приводил к его
 * переполнению/недозаполнению. Далее в прерывании, через опрос флагов
 * определяется в какую сторону был поворот.
 *------------------------------------------------------------------------------
 * НИИЭТ, Богдан Колбов <kolbov@niiet.ru>
 *==============================================================================
 * ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО
 * ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ ГАРАНТИИ ТОВАРНОЙ
 * ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ НАЗНАЧЕНИЮ И ОТСУТСТВИЯ
 * НАРУШЕНИЙ, НО НЕ ОГРАНИЧИВАЯСЬ ИМИ. ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ
 * ПРЕДНАЗНАЧЕНО ДЛЯ ОЗНАКОМИТЕЛЬНЫХ ЦЕЛЕЙ И НАПРАВЛЕНО ТОЛЬКО НА
 * ПРЕДОСТАВЛЕНИЕ ДОПОЛНИТЕЛЬНОЙ ИНФОРМАЦИИ О ПРОДУКТЕ, С ЦЕЛЬЮ СОХРАНИТЬ ВРЕМЯ
 * ПОТРЕБИТЕЛЮ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ
 * ОТВЕТСТВЕННОСТИ ПО КАКИМ-ЛИБО ИСКАМ, ЗА ПРЯМОЙ ИЛИ КОСВЕННЫЙ УЩЕРБ, ИЛИ
 * ПО ИНЫМ ТРЕБОВАНИЯМ, ВОЗНИКШИМ ИЗ-ЗА ИСПОЛЬЗОВАНИЯ ПРОГРАММНОГО ОБЕСПЕЧЕНИЯ
 * ИЛИ ИНЫХ ДЕЙСТВИЙ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.
 *
 *                              2018 АО "НИИЭТ"
 *==============================================================================
 */

//-- Includes ------------------------------------------------------------------
#include "plib035.h"
#include "retarget_conf.h"

//-- Defines -------------------------------------------------------------------
enum {
    ENC_NONE,
    ENC_INC,
    ENC_DEC
};

//-- Variables -----------------------------------------------------------------
volatile uint32_t enc_event;
volatile int enc_count;

//-- Peripheral init functions -------------------------------------------------
void gpio_init()
{
    RCU_AHBClkCmd(RCU_AHBClk_GPIOA, ENABLE);
    RCU_AHBRstCmd(RCU_AHBRst_GPIOA, ENABLE);
    GPIO_OutCmd(GPIOA, GPIO_Pin_15, ENABLE);
    GPIO_DigitalCmd(GPIOA, GPIO_Pin_15, ENABLE);
}

void qep_init()
{
    QEP_PC_Init_TypeDef QEP_PC_InitStruct;
    GPIO_Init_TypeDef GPIO_InitStruct;

    enc_event = ENC_NONE;

    //настраиваем пины
    QEP_RemapCmd(ENABLE);
    RCU_AHBClkCmd(RCU_AHBClk_GPIOB, ENABLE);
    RCU_AHBRstCmd(RCU_AHBRst_GPIOB, ENABLE);
    GPIO_StructInit(&GPIO_InitStruct);
    GPIO_InitStruct.AltFunc = ENABLE;
    GPIO_InitStruct.Pin = GPIO_Pin_12 | GPIO_Pin_13;
    GPIO_InitStruct.PullMode = GPIO_PullMode_PU;
    GPIO_Init(GPIOB, &GPIO_InitStruct);
    GPIO_DigitalCmd(GPIOB, GPIO_InitStruct.Pin, ENABLE);

    //включаем тактирование и выводим из сброса
    RCU_APBClkCmd(RCU_APBClk_QEP, ENABLE);
    RCU_APBRstCmd(RCU_APBRst_QEP, ENABLE);

    //настраиваем таймер
    QEP_PC_StructInit(&QEP_PC_InitStruct);
    QEP_PC_InitStruct.CountMax = 6;
    QEP_PC_InitStruct.CountInit = 3;
    QEP_PC_InitStruct.Mode = QEP_PC_Mode_Quad;
    QEP_PC_Init(&QEP_PC_InitStruct);
    QEP_PC_Cmd(ENABLE);

    //прерывания
    QEP_ITCmd(QEP_ITSource_PCOverflow | QEP_ITSource_PCUnderflow, ENABLE);
    NVIC_EnableIRQ(QEP_IRQn);
}
void periph_init()
{
    SystemCoreClockUpdate();
    retarget_init();
    gpio_init();
    qep_init();
    printf("\nAll peripherals inited, SYSCLK = %3d MHz\n", (int)(SystemCoreClock / 1E6));
}

//-- Main ----------------------------------------------------------------------

int main()
{
    periph_init();
    QEP_PC_SwInitCmd();
    enc_count = 0;

    while (1) {
        if (enc_event != ENC_NONE) {
            if ((enc_event == ENC_INC) && (enc_count != 255))
                enc_count++;
            else if ((enc_event == ENC_DEC) && (enc_count != 0))
                enc_count--;
            enc_event = ENC_NONE;
            printf("enc_count: %03d\n", enc_count);
        }
    }
    return 0;
}

//-- IRQ handlers --------------------------------------------------------------
void QEP_IRQHandler(void)
{
    GPIO_ToggleBits(GPIOA, GPIO_Pin_15);
    QEP_PC_SwInitCmd();
    if (QEP_ITStatus(QEP_ITSource_PCOverflow))
        enc_event = ENC_INC;
    else if (QEP_ITStatus(QEP_ITSource_PCUnderflow))
        enc_event = ENC_DEC;
    QEP_ITStatusClear(QEP_ITStatus_All);
    QEP_ITPendStatusClear();
}

//-- Assert --------------------------------------------------------------------
#if defined USE_FULL_ASSERT
void assert_failed(uint8_t* file, uint32_t line)
{
    printf("Assert failed: file %s on line %d\n", file, (int)line);
    while (1) {
    };
}
#endif /* USE_FULL_ASSERT */
