/*==============================================================================
 * Запускается преобразование 0 и 1 каналов.
 * Каждые 32 измерения вызывается прерывание и результаты выводятся в printf().
 * Ппроводится три серии измерений таким образом.
 *------------------------------------------------------------------------------
 * НИИЭТ, Богдан Колбов <kolbov@niiet.ru>
 *==============================================================================
 * ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО
 * ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ ГАРАНТИИ ТОВАРНОЙ
 * ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ НАЗНАЧЕНИЮ И ОТСУТСТВИЯ
 * НАРУШЕНИЙ, НО НЕ ОГРАНИЧИВАЯСЬ ИМИ. ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ
 * ПРЕДНАЗНАЧЕНО ДЛЯ ОЗНАКОМИТЕЛЬНЫХ ЦЕЛЕЙ И НАПРАВЛЕНО ТОЛЬКО НА
 * ПРЕДОСТАВЛЕНИЕ ДОПОЛНИТЕЛЬНОЙ ИНФОРМАЦИИ О ПРОДУКТЕ, С ЦЕЛЬЮ СОХРАНИТЬ ВРЕМЯ
 * ПОТРЕБИТЕЛЮ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ
 * ОТВЕТСТВЕННОСТИ ПО КАКИМ-ЛИБО ИСКАМ, ЗА ПРЯМОЙ ИЛИ КОСВЕННЫЙ УЩЕРБ, ИЛИ
 * ПО ИНЫМ ТРЕБОВАНИЯМ, ВОЗНИКШИМ ИЗ-ЗА ИСПОЛЬЗОВАНИЯ ПРОГРАММНОГО ОБЕСПЕЧЕНИЯ
 * ИЛИ ИНЫХ ДЕЙСТВИЙ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.
 *
 *                              2018 АО "НИИЭТ"
 *==============================================================================
 */

//-- Includes ------------------------------------------------------------------
#include "plib035.h"
#include "retarget_conf.h"

//-- Defines -------------------------------------------------------------------

//-- Peripheral init functions -------------------------------------------------
void adc_init()
{
    ADC_SEQ_Init_TypeDef ADC_SEQ_InitStruct;

    RCU_ADCClkConfig(RCU_PeriphClk_PLLClk, 7, ENABLE); //12.5MHz
    RCU_ADCClkCmd(ENABLE);
    RCU_ADCRstCmd(ENABLE);

    //Включаем модуль АЦП
    ADC_AM_Cmd(ENABLE);
    //Настройка секвенсора 0 (пока аналаговая часть инициализируется)
    ADC_SEQ_StructInit(&ADC_SEQ_InitStruct);
    ADC_SEQ_InitStruct.StartEvent = ADC_SEQ_StartEvent_SwReq;
    ADC_SEQ_InitStruct.SWStartEn = ENABLE;
    ADC_SEQ_InitStruct.Req[ADC_SEQ_ReqNum_0] = ADC_CH_Num_0;
    ADC_SEQ_InitStruct.Req[ADC_SEQ_ReqNum_1] = ADC_CH_Num_1;
    ADC_SEQ_InitStruct.ReqMax = ADC_SEQ_ReqNum_1;
    ADC_SEQ_InitStruct.RestartCount = 15;
    ADC_SEQ_Init(ADC_SEQ_Num_0, &ADC_SEQ_InitStruct);
    ADC_SEQ_Cmd(ADC_SEQ_Num_0, ENABLE);
    while (!ADC_AM_ReadyStatus()) {
    }

    ADC_SEQ_ITConfig(ADC_SEQ_Num_0, 31, DISABLE);
    ADC_SEQ_ITCmd(ADC_SEQ_Num_0, ENABLE);
    NVIC_EnableIRQ(ADC_SEQ0_IRQn);
}
void periph_init()
{
    SystemCoreClockUpdate();
    retarget_init();
    adc_init();
    printf("\nAll peripherals inited, SYSCLK = %3d MHz\n", (int)(SystemCoreClock / 1E6));
}

//-- Main ----------------------------------------------------------------------
volatile uint32_t adc_irq_count = 0;
int main()
{
    int ch2_res;
    int ch3_res;

    periph_init();
    printf("Start measures!\n");

    while (1) {
        if (adc_irq_count < 3) {
            ADC_SEQ_SwStartCmd();
            __WFI();
            printf("\nMeasure results:\n");
            printf(" CH0     CH1\n");
            while (ADC_SEQ_GetFIFOLoad(ADC_SEQ_Num_0)) {
                ch2_res = ADC_SEQ_GetFIFOData(ADC_SEQ_Num_0);
                ch3_res = ADC_SEQ_GetFIFOData(ADC_SEQ_Num_0);
                printf("%04d,   %04d\n", ch2_res, ch3_res);
            }
        }
    }
}

//-- IRQ handlers --------------------------------------------------------------
void ADC_SEQ0_IRQHandler()
{
    ADC_SEQ_ITStatusClear(ADC_SEQ_Num_0);
    adc_irq_count++;
}

//-- Assert --------------------------------------------------------------------
#if defined USE_FULL_ASSERT
void assert_failed(uint8_t* file, uint32_t line)
{
    printf("Assert failed: file %s on line %d\n", file, (int)line);
    while (1) {
    };
}
#endif /* USE_FULL_ASSERT */
