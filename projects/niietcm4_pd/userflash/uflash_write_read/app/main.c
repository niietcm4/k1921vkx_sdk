/**
  ******************************************************************************
  * @file    main.c
  *
  * @brief   Пример выполнения операций с основной пользовательской флеш:
  *          чтение, запись, стирание.
  *
  *          В примере читаются из пользовательской флеш и выводятся в терминал
  *          8 байт. Затем происходит запись новых данных во флеш. Обновленные
  *          данные считываются, а затем происходит стирание страницы с
  *          модифицированными адресами.
  *
  *          Компилятор: GCC ARM 4.9.3
  *          Среда: Qt Creator 3.4.2
  *
  * @author  НИИЭТ
  *             - Богдан Колбов (bkolbov), kolbov@niiet.ru
  * @date    07.12.2015
  *
  ******************************************************************************
  * @attention
  *
  * ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО
  * ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ ГАРАНТИИ ТОВАРНОЙ
  * ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ НАЗНАЧЕНИЮ И ОТСУТСТВИЯ
  * НАРУШЕНИЙ, НО НЕ ОГРАНИЧИВАЯСЬ ИМИ. ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ
  * ПРЕДНАЗНАЧЕНО ДЛЯ ОЗНАКОМИТЕЛЬНЫХ ЦЕЛЕЙ И НАПРАВЛЕНО ТОЛЬКО НА
  * ПРЕДОСТАВЛЕНИЕ ДОПОЛНИТЕЛЬНОЙ ИНФОРМАЦИИ О ПРОДУКТЕ, С ЦЕЛЬЮ СОХРАНИТЬ ВРЕМЯ
  * ПОТРЕБИТЕЛЮ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ
  * ОТВЕТСТВЕННОСТИ ПО КАКИМ-ЛИБО ИСКАМ, ЗА ПРЯМОЙ ИЛИ КОСВЕННЫЙ УЩЕРБ, ИЛИ
  * ПО ИНЫМ ТРЕБОВАНИЯМ, ВОЗНИКШИМ ИЗ-ЗА ИСПОЛЬЗОВАНИЯ ПРОГРАММНОГО ОБЕСПЕЧЕНИЯ
  * ИЛИ ИНЫХ ДЕЙСТВИЙ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.
  *
  * <h2><center>&copy; 2015 ОАО "НИИЭТ"</center></h2>
  ******************************************************************************
  */

#include "niietcm4.h"
#include "system_K1921VK01T.h"
#include "retarget_conf.h"

void SystemInit()
{}

void PeriphInit()
{
    // инициализация printf
    retarget_init();

    USERFLASH_Init(EXT_OSC_VALUE);
}

int main()
{
    PeriphInit();


    uint32_t addr = 0x00000203;

    // читаем значения до всех операций
    printf("Read flash before operation:\n");
    for (uint32_t i = 0; i < 8; i++)
    {
       printf("%d - 0x%x - 0x%x\n", i, addr + i, USERFLASH_Read(addr + i));
    }
    USERFLASH_OperationStatusClear();   // сбрасываем флаги, оставшиеся после последнего чтения

    // пишем значения
    printf("Performing write...\n");
    for (uint32_t i = 0; i < 8; i++)
    {
       USERFLASH_Write(addr + i, i*2+1);
       // ждем выполнения
       while(USERFLASH_OperationStatus() != USERFLASH_Status_Complete);
       USERFLASH_OperationStatusClear();
    }

    // читаем и выводим новые значения
    printf("Write operation complete!\n");
    printf("Current values:\n");
    for (uint32_t i = 0; i < 8; i++)
    {
       printf("%d - 0x%x - 0x%x\n", i, addr + i, USERFLASH_Read(addr + i));
    }
    USERFLASH_OperationStatusClear();   // сбрасываем флаги, оставшиеся после последнего чтения

    // стираем страницу, куда писали
    printf("Performing page erase...\n");
    USERFLASH_PageErase(addr/USERFLASH_PAGE_SIZE_BYTES);

    // ждем выполнения
    while(USERFLASH_OperationStatus() != USERFLASH_Status_Complete);
    USERFLASH_OperationStatusClear();

    // читаем и выводим новые значения
    printf("Erase operation complete!\n");
    printf("Current values:\n");
    for (uint32_t i = 0; i < 8; i++)
    {
       printf("%d - 0x%x - 0x%x\n", i, addr + i, USERFLASH_Read(addr + i));
    }
    USERFLASH_OperationStatusClear();   // сбрасываем флаги, оставшиеся после последнего чтения

    printf("The end!\n");
    while(1);
}

