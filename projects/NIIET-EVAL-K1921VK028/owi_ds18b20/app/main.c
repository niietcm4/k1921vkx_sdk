/*==============================================================================
 * Получение значения температуры сдатчика DS18B20 с помощью
 * однопроводного интерфейса OWI каждые 2 секунды
 *------------------------------------------------------------------------------
 * НИИЭТ, Дмитрий Сериков <lonie@niiet.ru>
 * НИИЭТ, Богдан Колбов <kolbov@niiet.ru>
 *==============================================================================
 * ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО
 * ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ ГАРАНТИИ ТОВАРНОЙ
 * ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ НАЗНАЧЕНИЮ И ОТСУТСТВИЯ
 * НАРУШЕНИЙ, НО НЕ ОГРАНИЧИВАЯСЬ ИМИ. ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ
 * ПРЕДНАЗНАЧЕНО ДЛЯ ОЗНАКОМИТЕЛЬНЫХ ЦЕЛЕЙ И НАПРАВЛЕНО ТОЛЬКО НА
 * ПРЕДОСТАВЛЕНИЕ ДОПОЛНИТЕЛЬНОЙ ИНФОРМАЦИИ О ПРОДУКТЕ, С ЦЕЛЬЮ СОХРАНИТЬ ВРЕМЯ
 * ПОТРЕБИТЕЛЮ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ
 * ОТВЕТСТВЕННОСТИ ПО КАКИМ-ЛИБО ИСКАМ, ЗА ПРЯМОЙ ИЛИ КОСВЕННЫЙ УЩЕРБ, ИЛИ
 * ПО ИНЫМ ТРЕБОВАНИЯМ, ВОЗНИКШИМ ИЗ-ЗА ИСПОЛЬЗОВАНИЯ ПРОГРАММНОГО ОБЕСПЕЧЕНИЯ
 * ИЛИ ИНЫХ ДЕЙСТВИЙ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.
 *
 *                              2019 АО "НИИЭТ"
 *==============================================================================
 */

//-- Includes ------------------------------------------------------------------
#include "K1921VK028.h"
#include "bsp.h"
#include "retarget_conf.h"

//-- Defines -------------------------------------------------------------------

//-- Variables -----------------------------------------------------------------
volatile uint32_t ds18b20_fsm = 0;

struct {
    union {
        volatile uint16_t value16;
        struct {
            volatile uint16_t f : 4;
            volatile uint16_t i : 12;
        } value;
        volatile uint8_t value8[2];
    } temp;
} ds18b20;

//-- Special peripheral functions ----------------------------------------------
uint32_t OWI_ResetLine(void)
{
    OWI0->CMD_bit.RSTCMD = 1;
    while (OWI0->STAT_bit.BUSY) {
    };
    return (uint32_t)(OWI0->STAT_bit.PRES);
}

void OWI_SendData(uint8_t data)
{
    OWI0->DATA = data;
    while (OWI0->STAT_bit.BUSY) {
    };
}

uint8_t OWI_RecvData(void)
{
    OWI0->CMD_bit.RDCMD = 1;
    while (OWI0->STAT_bit.BUSY) {
    };
    return (uint8_t)OWI0->DATA;
}

//-- Peripheral init functions -------------------------------------------------
void OWI_Init(void)
{
    RCU->HCLKCFG_bit.GPIODEN = 1;
    RCU->HRSTCFG_bit.GPIODEN = 1;
    GPIOD->ALTFUNCNUM0_bit.PIN0 = 1;                  // en AF1
    GPIOD->ALTFUNCSET = GPIO_ALTFUNCSET_PIN0_Msk;     // en AF
    GPIOD->DENSET = GPIO_DENSET_PIN0_Msk;             // en port
    GPIOD->PULLMODE_bit.PIN0 = GPIO_PULLMODE_PIN0_PU; // en pullup

    RCU->PCLKCFG1_bit.OWI0EN = 1;
    RCU->PRSTCFG1_bit.OWI0EN = 1;
    OWI0->CTRL0_bit.EN = 1;
    OWI0->CTRL0_bit.DIV = 700;    // 7us @ 100MHz
    OWI0->CTRL1_bit.OBPER = 9;    // ~60 us
    OWI0->CTRL1_bit.RSTPER = 72;  // ~500 us
    OWI0->CTRL1_bit.PRESPER = 12; // ~100 us
}

void periph_init()
{
    SystemCoreClockUpdate();
    retarget_init();
    OWI_Init();
    BSP_LED_Init();
    printf("\nAll peripherals inited, SYSCLK = %3d MHz\n", (int)(SystemCoreClock / 1E6));
}

//-- Main ----------------------------------------------------------------------
int main()
{
    periph_init();
    SysTick_Config(SystemCoreClock / 1000 - 1); // 1ms tick

    while (1) {
        switch (ds18b20_fsm) {
        case 1: //in idle state
            if (OWI_ResetLine() != 0) {
                OWI_SendData(0xCC); // skip rom
                OWI_SendData(0x44); // conv t
            } else
                BSP_LED_On(LED1_MSK);
            break;
        case 2: // read conv temp
            if (OWI_ResetLine() != 0) {
                OWI_SendData(0xCC); // skip rom
                OWI_SendData(0xBE); // read scratchpad

                ds18b20.temp.value8[0] = OWI_RecvData();
                ds18b20.temp.value8[1] = OWI_RecvData();
                ds18b20_fsm = 0;

                float temp = ds18b20.temp.value.f ? (float)(ds18b20.temp.value.i + 1.0f / ds18b20.temp.value.f) : (float)(ds18b20.temp.value.i);
                printf("Temperature = %f (0x%04x)\n", temp, ds18b20.temp.value16);
            } else
                BSP_LED_On(LED1_MSK);
            ds18b20_fsm = 0;
            break;
        }
    };
    return 0;
}

//-- IRQ handlers --------------------------------------------------------------
volatile uint32_t tick_counter = 0;
void SysTick_Handler()
{
    if (tick_counter < 1000)
        tick_counter++;
    else {
        ds18b20_fsm++;
        tick_counter = 0;
        BSP_LED_Toggle(LED0_MSK);
    }
}
