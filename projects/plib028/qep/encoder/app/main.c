/*==============================================================================
 * Реализация обработки вращения инкрементального энкодера.
 * Счетчик позиции настраивается так, чтобы один щелчок энкодера приводил к его
 * переполнению/недозаполнению. Далее в прерывании, через опрос флагов
 * определяется в какую сторону был поворот.
 *------------------------------------------------------------------------------
 * НИИЭТ, Богдан Колбов <kolbov@niiet.ru>
 *==============================================================================
 * ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО
 * ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ ГАРАНТИИ ТОВАРНОЙ
 * ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ НАЗНАЧЕНИЮ И ОТСУТСТВИЯ
 * НАРУШЕНИЙ, НО НЕ ОГРАНИЧИВАЯСЬ ИМИ. ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ
 * ПРЕДНАЗНАЧЕНО ДЛЯ ОЗНАКОМИТЕЛЬНЫХ ЦЕЛЕЙ И НАПРАВЛЕНО ТОЛЬКО НА
 * ПРЕДОСТАВЛЕНИЕ ДОПОЛНИТЕЛЬНОЙ ИНФОРМАЦИИ О ПРОДУКТЕ, С ЦЕЛЬЮ СОХРАНИТЬ ВРЕМЯ
 * ПОТРЕБИТЕЛЮ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ
 * ОТВЕТСТВЕННОСТИ ПО КАКИМ-ЛИБО ИСКАМ, ЗА ПРЯМОЙ ИЛИ КОСВЕННЫЙ УЩЕРБ, ИЛИ
 * ПО ИНЫМ ТРЕБОВАНИЯМ, ВОЗНИКШИМ ИЗ-ЗА ИСПОЛЬЗОВАНИЯ ПРОГРАММНОГО ОБЕСПЕЧЕНИЯ
 * ИЛИ ИНЫХ ДЕЙСТВИЙ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.
 *
 *                              2019 АО "НИИЭТ"
 *==============================================================================
 */

//-- Includes ------------------------------------------------------------------
#include "plib028.h"
#include "retarget_conf.h"

//-- Defines -------------------------------------------------------------------
enum {
    ENC_NONE,
    ENC_INC,
    ENC_DEC
};

//-- Variables -----------------------------------------------------------------
volatile uint32_t enc_event;
volatile int enc_count;

//-- Peripheral init functions -------------------------------------------------
void gpio_init()
{
    RCU_AHBClkCmd(RCU_AHBClk_GPIOA, ENABLE);
    RCU_AHBRstCmd(RCU_AHBRst_GPIOA, ENABLE);
    GPIO_OutCmd(GPIOA, GPIO_Pin_7_0, ENABLE);
    GPIO_DigitalCmd(GPIOA, GPIO_Pin_7_0, ENABLE);
    GPIO_SetBits(GPIOA, GPIO_Pin_7_0);
}

void qep_init()
{
    QEP_PC_Init_TypeDef QEP_PC_InitStruct;
    GPIO_Init_TypeDef GPIO_InitStruct;

    enc_event = ENC_NONE;

    //настраиваем пины
    RCU_AHBClkCmd(RCU_AHBClk_GPIOA, ENABLE);
    RCU_AHBRstCmd(RCU_AHBRst_GPIOA, ENABLE);
    GPIO_StructInit(&GPIO_InitStruct);
    GPIO_InitStruct.AltFuncNum = GPIO_AltFuncNum_1;
    GPIO_InitStruct.AltFunc = ENABLE;
    GPIO_InitStruct.Pin = GPIO_Pin_12 | GPIO_Pin_13;
    GPIO_InitStruct.PullMode = GPIO_PullMode_PU;
    GPIO_Init(GPIOA, &GPIO_InitStruct);
    GPIO_DigitalCmd(GPIOA, GPIO_InitStruct.Pin, ENABLE);

    //включаем тактирование и выводим из сброса
    RCU_APBClk1Cmd(RCU_APBClk1_QEP0, ENABLE);
    RCU_APBRst1Cmd(RCU_APBRst1_QEP0, ENABLE);

    //настраиваем таймер
    QEP_PC_StructInit(&QEP_PC_InitStruct);
    QEP_PC_InitStruct.CountMax = 6;
    QEP_PC_InitStruct.CountInit = 3;
    QEP_PC_InitStruct.Mode = QEP_PC_Mode_Quad;
    QEP_PC_Init(QEP0, &QEP_PC_InitStruct);
    QEP_PC_Cmd(QEP0, ENABLE);

    //прерывания
    QEP_ITCmd(QEP0, QEP_ITSource_PCOverflow | QEP_ITSource_PCUnderflow, ENABLE);
    NVIC_EnableIRQ(QEP0_IRQn);
}
void periph_init()
{
    SystemCoreClockUpdate();
    retarget_init();
    gpio_init();
    gpio_init();
    qep_init();
    printf("\nAll peripherals inited, SYSCLK = %3d MHz\n", (int)(SystemCoreClock / 1E6));
}

//-- Main ----------------------------------------------------------------------

int main()
{
    periph_init();
    SysTick_Config(SystemCoreClock / 1000 - 1); //1ms tick
    QEP_PC_SwInitCmd(QEP0);
    enc_count = 0;

    while (1) {
        if (enc_event != ENC_NONE) {
            if ((enc_event == ENC_INC) && (enc_count != 255))
                enc_count++;
            else if ((enc_event == ENC_DEC) && (enc_count != 0))
                enc_count--;
            enc_event = ENC_NONE;
            printf("enc_count: %03d\n", enc_count);
            GPIO_WriteLowMask(GPIOA, 0xFF, enc_count);
        }
    }
    return 0;
}

//-- IRQ handlers --------------------------------------------------------------
volatile uint32_t tick_counter = 0;
void SysTick_Handler()
{
    if (tick_counter >= 200) {
        tick_counter = 0;
        GPIO_ToggleBits(GPIOA, GPIO_Pin_0);
    } else
        tick_counter++;
}
void QEP0_IRQHandler(void)
{
    QEP_PC_SwInitCmd(QEP0);
    if (QEP_ITStatus(QEP0, QEP_ITSource_PCOverflow))
        enc_event = ENC_INC;
    else if (QEP_ITStatus(QEP0,QEP_ITSource_PCUnderflow))
        enc_event = ENC_DEC;
    QEP_ITStatusClear(QEP0,QEP_ITStatus_All);
    QEP_ITPendStatusClear(QEP0);
}

//-- Assert --------------------------------------------------------------------
#if defined USE_FULL_ASSERT
void assert_failed(uint8_t* file, uint32_t line)
{
    printf("Assert failed: file %s on line %d\n", file, (int)line);
    while (1) {
    };
}
#endif /* USE_FULL_ASSERT */
