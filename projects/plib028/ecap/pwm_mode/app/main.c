/*==============================================================================
 * Пример, использующий функции драйвера CAP, чтобы организовать
 * 2 ШИМ сигнала, генерирующиеся в противофазе.
 *------------------------------------------------------------------------------
 * НИИЭТ, Богдан Колбов <kolbov@niiet.ru>
 *==============================================================================
 * ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО
 * ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ ГАРАНТИИ ТОВАРНОЙ
 * ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ НАЗНАЧЕНИЮ И ОТСУТСТВИЯ
 * НАРУШЕНИЙ, НО НЕ ОГРАНИЧИВАЯСЬ ИМИ. ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ
 * ПРЕДНАЗНАЧЕНО ДЛЯ ОЗНАКОМИТЕЛЬНЫХ ЦЕЛЕЙ И НАПРАВЛЕНО ТОЛЬКО НА
 * ПРЕДОСТАВЛЕНИЕ ДОПОЛНИТЕЛЬНОЙ ИНФОРМАЦИИ О ПРОДУКТЕ, С ЦЕЛЬЮ СОХРАНИТЬ ВРЕМЯ
 * ПОТРЕБИТЕЛЮ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ
 * ОТВЕТСТВЕННОСТИ ПО КАКИМ-ЛИБО ИСКАМ, ЗА ПРЯМОЙ ИЛИ КОСВЕННЫЙ УЩЕРБ, ИЛИ
 * ПО ИНЫМ ТРЕБОВАНИЯМ, ВОЗНИКШИМ ИЗ-ЗА ИСПОЛЬЗОВАНИЯ ПРОГРАММНОГО ОБЕСПЕЧЕНИЯ
 * ИЛИ ИНЫХ ДЕЙСТВИЙ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.
 *
 *                              2019 АО "НИИЭТ"
 *==============================================================================
 */

//-- Includes ------------------------------------------------------------------
#include "plib028.h"
#include "retarget_conf.h"

//-- Defines -------------------------------------------------------------------

//-- Peripheral init functions -------------------------------------------------
void gpio_init()
{
    RCU_AHBClkCmd(RCU_AHBClk_GPIOA, ENABLE);
    RCU_AHBRstCmd(RCU_AHBRst_GPIOA, ENABLE);
    GPIO_OutCmd(GPIOA, GPIO_Pin_7_0, ENABLE);
    GPIO_DigitalCmd(GPIOA, GPIO_Pin_7_0, ENABLE);
    GPIO_SetBits(GPIOA, GPIO_Pin_7_0);
}
void ecap_init()
{
    // инициализация блоков захвата
    RCU_APBClk0Cmd(RCU_APBClk0_ECAP1, ENABLE);
    RCU_APBRst0Cmd(RCU_APBRst0_ECAP1, ENABLE);
    RCU_APBClk0Cmd(RCU_APBClk0_ECAP2, ENABLE);
    RCU_APBRst0Cmd(RCU_APBRst0_ECAP2, ENABLE);

    // выбираем PWM режим
    ECAP_Init_TypeDef ECAP_InitStruct;
    ECAP_StructInit(&ECAP_InitStruct);
    ECAP_InitStruct.Mode = ECAP_Mode_PWM;
    ECAP_InitStruct.SyncEn = ENABLE;
    ECAP_Init(ECAP1, &ECAP_InitStruct);
    ECAP_Init(ECAP2, &ECAP_InitStruct);

    // настраиваем PWM режим
    // 2 меандра в противофазе
    ECAP_PWM_Init_TypeDef ECAP_PWM_InitStruct;
    ECAP_PWM_StructInit(&ECAP_PWM_InitStruct);
    ECAP_PWM_InitStruct.Period = 0x3000;
    ECAP_PWM_InitStruct.Compare = 0x1800;
    ECAP_PWM_InitStruct.Polarity = ECAP_PWM_Polarity_Pos;
    ECAP_PWM_Init(ECAP1, &ECAP_PWM_InitStruct);
    ECAP_PWM_InitStruct.Polarity = ECAP_PWM_Polarity_Neg;
    ECAP_PWM_Init(ECAP2, &ECAP_PWM_InitStruct);

    // включаем счетчики - ШИМ начинает генерировать
    ECAP_TimerCmd(ECAP1, ENABLE);
    ECAP_TimerCmd(ECAP2, ENABLE);

    // синхронизируем счетчики для точной противофазы
    // в оба счетчика запишутся нули
    ECAP_SwSync(ECAP1);

    // инициализация выводов блоков ECAP1, ECAP2
    RCU_AHBClkCmd(RCU_AHBClk_GPIOB, ENABLE);
    RCU_AHBRstCmd(RCU_AHBRst_GPIOB, ENABLE);
    GPIO_Init_TypeDef GPIO_InitStruct;
    GPIO_StructInit(&GPIO_InitStruct);
    GPIO_InitStruct.AltFuncNum = GPIO_AltFuncNum_1;
    GPIO_InitStruct.AltFunc = ENABLE;
    GPIO_InitStruct.Pin = GPIO_Pin_2 | GPIO_Pin_1;
    GPIO_Init(GPIOB, &GPIO_InitStruct);
    GPIO_DigitalCmd(GPIOB, GPIO_Pin_2 | GPIO_Pin_1, ENABLE);
}
void periph_init()
{
    SystemCoreClockUpdate();
    retarget_init();
    gpio_init();
    ecap_init();
    printf("\nAll peripherals inited, SYSCLK = %3d MHz\n", (int)(SystemCoreClock / 1E6));
}

//-- Main ----------------------------------------------------------------------
int main()
{
    periph_init();
    SysTick_Config(SystemCoreClock / 1000 - 1); //1ms tick
    printf("PWM generating on B1, B2\n");

    while (1) {
    };
    return 0;
}

//-- IRQ handlers --------------------------------------------------------------
volatile uint32_t tick_counter = 0;
void SysTick_Handler()
{
    if (tick_counter >= 200) {
        tick_counter = 0;
        GPIO_ToggleBits(GPIOA, GPIO_Pin_0);
    } else
        tick_counter++;
}

//-- Assert --------------------------------------------------------------------
#if defined USE_FULL_ASSERT
void assert_failed(uint8_t* file, uint32_t line)
{
    printf("Assert failed: file %s on line %d\n", file, (int)line);
    while (1) {
    };
}
#endif /* USE_FULL_ASSERT */
