/*==============================================================================
 * Пример, использующий функции драйвера ECAP, чтобы организовать измерение
 * частоты входного прямоугольного сигнала
 *------------------------------------------------------------------------------
 * НИИЭТ, Богдан Колбов <kolbov@niiet.ru>
 *==============================================================================
 * ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО
 * ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ ГАРАНТИИ ТОВАРНОЙ
 * ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ НАЗНАЧЕНИЮ И ОТСУТСТВИЯ
 * НАРУШЕНИЙ, НО НЕ ОГРАНИЧИВАЯСЬ ИМИ. ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ
 * ПРЕДНАЗНАЧЕНО ДЛЯ ОЗНАКОМИТЕЛЬНЫХ ЦЕЛЕЙ И НАПРАВЛЕНО ТОЛЬКО НА
 * ПРЕДОСТАВЛЕНИЕ ДОПОЛНИТЕЛЬНОЙ ИНФОРМАЦИИ О ПРОДУКТЕ, С ЦЕЛЬЮ СОХРАНИТЬ ВРЕМЯ
 * ПОТРЕБИТЕЛЮ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ
 * ОТВЕТСТВЕННОСТИ ПО КАКИМ-ЛИБО ИСКАМ, ЗА ПРЯМОЙ ИЛИ КОСВЕННЫЙ УЩЕРБ, ИЛИ
 * ПО ИНЫМ ТРЕБОВАНИЯМ, ВОЗНИКШИМ ИЗ-ЗА ИСПОЛЬЗОВАНИЯ ПРОГРАММНОГО ОБЕСПЕЧЕНИЯ
 * ИЛИ ИНЫХ ДЕЙСТВИЙ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.
 *
 *                              2018 АО "НИИЭТ"
 *==============================================================================
 */

//-- Includes ------------------------------------------------------------------
#include "plib035.h"
#include "retarget_conf.h"

//-- Defines -------------------------------------------------------------------

//-- Peripheral init functions -------------------------------------------------
void ecap_init()
{
    // инициализация блока захвата
    RCU_APBClkCmd(RCU_APBClk_ECAP2, ENABLE);
    RCU_APBRstCmd(RCU_APBRst_ECAP2, ENABLE);

    // выбираем Capture режим
    ECAP_Init_TypeDef ECAP_InitStruct;
    ECAP_StructInit(&ECAP_InitStruct);
    ECAP_InitStruct.Mode = ECAP_Mode_Capture;
    ECAP_Init(ECAP2, &ECAP_InitStruct);

    // настраиваем Capture режим
    ECAP_Capture_Init_TypeDef ECAP_Capture_InitStruct;
    ECAP_Capture_StructInit(&ECAP_Capture_InitStruct);
    ECAP_Capture_InitStruct.Mode = ECAP_Capture_Mode_Cycle;
    ECAP_Capture_InitStruct.PolarityEvt0 = ECAP_Capture_Polarity_PosEdge;
    ECAP_Capture_InitStruct.RstEvt0 = ENABLE;
    ECAP_Capture_Init(ECAP2, &ECAP_Capture_InitStruct);

    // включаем прерывание по событию, чтобы считывать флаги
    ECAP_ITCmd(ECAP2, ECAP_ITSource_CapEvt0, ENABLE);

    // инициализация выводов блоков ECAP2
    // так как вывод соответсвует выводу JTAG - он уже проинициализирован и включен по умолчанию,
    // нужно только включить ремап функции
    ECAP_RemapCmd(ECAP2, ENABLE);
}
void periph_init()
{
    SystemCoreClockUpdate();
    retarget_init();
    ecap_init();
    printf("\nAll peripherals inited, SYSCLK = %3d MHz\n", (int)(SystemCoreClock / 1E6));
}

//-- Main ----------------------------------------------------------------------
int main()
{
    periph_init();
    printf("External signal frequency:\n");
    ECAP_TimerCmd(ECAP2, ENABLE);
    ECAP_Capture_Cmd(ECAP2, ENABLE);

    while (1) {
        if (ECAP_ITStatus(ECAP2, ECAP_ITStatus_CapEvt0)) {
            ECAP_ITStatusClear(ECAP2, ECAP_ITStatus_CapEvt0);
            ECAP_ITStatusClear(ECAP2, ECAP_ITStatus_GeneralInt);
            ECAP_ITPendStatusClear(ECAP2);
            printf("%d Hz     \r", (int)(SystemCoreClock / ECAP_Capture_GetCap0(ECAP2)));
            for (uint32_t i = 0; i < 100000; i++)
                __NOP();
        }
    };
    return 0;
}

//-- IRQ handlers --------------------------------------------------------------

//-- Assert --------------------------------------------------------------------
#if defined USE_FULL_ASSERT
void assert_failed(uint8_t* file, uint32_t line)
{
    printf("Assert failed: file %s on line %d\n", file, (int)line);
    while (1) {
    };
}
#endif /* USE_FULL_ASSERT */
