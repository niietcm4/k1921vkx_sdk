/**
  ******************************************************************************
  * @file    main.c
  *
  * @brief   Пример, использующий функции драйвера таймеров и модуль назначения
  *          обработчика прерываний в runtime.
  *
  *          TIMER0 настраивается на прерывание каждые  100 мс.
  *          В прерывании переключается состояние вывода E[8]. Обработчик,
  *          в котором будет происходить переключение назначается в ходе
  *          выполнения программы.
  *
  *          Компилятор: GCC ARM 4.9.3
  *          Среда: Qt Creator 3.4.2
  *
  * @author  НИИЭТ
  *             - Богдан Колбов (bkolbov), kolbov@niiet.ru
  * @date    03.08.2016
  *
  ******************************************************************************
  * @attention
  *
  * ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО
  * ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ ГАРАНТИИ ТОВАРНОЙ
  * ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ НАЗНАЧЕНИЮ И ОТСУТСТВИЯ
  * НАРУШЕНИЙ, НО НЕ ОГРАНИЧИВАЯСЬ ИМИ. ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ
  * ПРЕДНАЗНАЧЕНО ДЛЯ ОЗНАКОМИТЕЛЬНЫХ ЦЕЛЕЙ И НАПРАВЛЕНО ТОЛЬКО НА
  * ПРЕДОСТАВЛЕНИЕ ДОПОЛНИТЕЛЬНОЙ ИНФОРМАЦИИ О ПРОДУКТЕ, С ЦЕЛЬЮ СОХРАНИТЬ ВРЕМЯ
  * ПОТРЕБИТЕЛЮ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ
  * ОТВЕТСТВЕННОСТИ ПО КАКИМ-ЛИБО ИСКАМ, ЗА ПРЯМОЙ ИЛИ КОСВЕННЫЙ УЩЕРБ, ИЛИ
  * ПО ИНЫМ ТРЕБОВАНИЯМ, ВОЗНИКШИМ ИЗ-ЗА ИСПОЛЬЗОВАНИЯ ПРОГРАММНОГО ОБЕСПЕЧЕНИЯ
  * ИЛИ ИНЫХ ДЕЙСТВИЙ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.
  *
  * <h2><center>&copy; 2016 ОАО "НИИЭТ"</center></h2>
  ******************************************************************************
  */

#include "niietcm4.h"
#include "system_K1921VK01T.h"


static void blink(void)
{
    GPIO_ToggleBits(NT_GPIOE, GPIO_Pin_8);
    TIMER_ITStatusClear(NT_TIMER0);
}

void SystemInit()
{}

void PeriphInit()
{
    // настройка E[8] на выход
    GPIO_Init_TypeDef GPIO_InitStruct;
    GPIO_StructInit(&GPIO_InitStruct);
    GPIO_InitStruct.GPIO_Dir = GPIO_Dir_Out;
    GPIO_InitStruct.GPIO_Out = GPIO_Out_En;
    GPIO_InitStruct.GPIO_Pin = GPIO_Pin_8;
    GPIO_Init(NT_GPIOE, &GPIO_InitStruct);
    GPIO_SetBits(NT_GPIOE, GPIO_Pin_8);

    // инициализация таймера
    IRQ_HandlerInit(TIM0_IRQn, blink);
    RCC_PeriphRstCmd(RCC_PeriphRst_Timer0, ENABLE);
    TIMER_PeriodConfig(NT_TIMER0, EXT_OSC_VALUE, 100000);
    TIMER_ITCmd(NT_TIMER0, ENABLE);
    TIMER_Cmd(NT_TIMER0, ENABLE);
    NVIC_EnableIRQ(TIM0_IRQn);
}

int main()
{
    PeriphInit();


    while(1);
}
