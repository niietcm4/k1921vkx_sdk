/*==============================================================================
 * Запуск RTC и выдача текущего времени каждую секунду
 *------------------------------------------------------------------------------
 * НИИЭТ, Богдан Колбов <kolbov@niiet.ru>
 *==============================================================================
 * ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО
 * ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ ГАРАНТИИ ТОВАРНОЙ
 * ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ НАЗНАЧЕНИЮ И ОТСУТСТВИЯ
 * НАРУШЕНИЙ, НО НЕ ОГРАНИЧИВАЯСЬ ИМИ. ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ
 * ПРЕДНАЗНАЧЕНО ДЛЯ ОЗНАКОМИТЕЛЬНЫХ ЦЕЛЕЙ И НАПРАВЛЕНО ТОЛЬКО НА
 * ПРЕДОСТАВЛЕНИЕ ДОПОЛНИТЕЛЬНОЙ ИНФОРМАЦИИ О ПРОДУКТЕ, С ЦЕЛЬЮ СОХРАНИТЬ ВРЕМЯ
 * ПОТРЕБИТЕЛЮ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ
 * ОТВЕТСТВЕННОСТИ ПО КАКИМ-ЛИБО ИСКАМ, ЗА ПРЯМОЙ ИЛИ КОСВЕННЫЙ УЩЕРБ, ИЛИ
 * ПО ИНЫМ ТРЕБОВАНИЯМ, ВОЗНИКШИМ ИЗ-ЗА ИСПОЛЬЗОВАНИЯ ПРОГРАММНОГО ОБЕСПЕЧЕНИЯ
 * ИЛИ ИНЫХ ДЕЙСТВИЙ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.
 *
 *                              2018 АО "НИИЭТ"
 *==============================================================================
 */

//-- Includes ------------------------------------------------------------------
#include "K1921VK028.h"
#include "bsp.h"
#include "retarget_conf.h"

//-- Defines -------------------------------------------------------------------

//-- Misc functions ------------------------------------------------------------
uint32_t bcd2hex(uint32_t a)
{
    if ((a & 0x000000F0) == 0)
        return (a & 0x0000000F);
    else
        return ((a & 0x0000000F) + 10 * ((a & 0x000000F0) >> 4));
}

uint32_t hex2bcd(uint32_t x)
{
    uint32_t y;
    y = (x / 10) << 4;
    y = y | (x % 10);
    return (y);
}

void print_time()
{
    uint32_t time;

    RTC->SHDW = 0;
    time = RTC->TIME;
    RTC->SHDW = RTC_SHDW_UPDTEN_Msk;

    printf("Time: %d:%d:%d\n",
           (int)bcd2hex(((time & RTC_TIME_HOUR_Msk) >> RTC_TIME_HOUR_Pos)),
           (int)bcd2hex(((time & RTC_TIME_MIN_Msk) >> RTC_TIME_MIN_Pos)),
           (int)bcd2hex(((time & RTC_TIME_SEC_Msk) >> RTC_TIME_SEC_Pos)));
}

void print_date()
{
    uint32_t weekday, day, month, year;
    const char* weekday_names[7] = { "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun" };

    RTC->SHDW = 0;
    weekday = bcd2hex(RTC->DOW);
    day = bcd2hex(RTC->DAY);
    month = bcd2hex(RTC->MONTH);
    year = bcd2hex(RTC->YEAR);
    RTC->SHDW = RTC_SHDW_UPDTEN_Msk;

    printf("Date: %s, %d.%d.%d\n", weekday_names[weekday - 1], (int)day, (int)month, (int)(2000 + year));
}

//-- Peripheral init functions -------------------------------------------------
void rtc_init()
{
    uint32_t is_configured = 0;
    uint32_t timeout_counter = 0;

    RCU->PCLKCFG0_bit.RTCEN = 1;
    RCU->PRSTCFG0_bit.RTCEN = 1;

    //ждём обновления в теневые регистры
    while (!is_configured && (timeout_counter < 1000000)) {
        is_configured = (bcd2hex(RTC->YEAR) == 15) ? 1 : 0;
        timeout_counter++;
    }

    if (!is_configured) {
        RTC->HOUR = 0x07;
        RTC->MIN = 0x28;
        RTC->SEC = 0x00;
        RTC->DOW = 0x3;
        RTC->DAY = 0x21;
        RTC->MONTH = 0x10;
        RTC->YEAR = 0x15;
    }
}

void periph_init()
{
    SystemCoreClockUpdate();
    retarget_init();
    rtc_init();
    BSP_LED_Init();
    printf("\nAll peripherals inited, SYSCLK = %3d MHz\n", (int)(SystemCoreClock / 1E6));
}

//-- Main ----------------------------------------------------------------------
int main()
{
    periph_init();
    SysTick_Config(10000000);
    print_date();
    NVIC_EnableIRQ(RTC_IRQn);

    while (1) {
    };
    return 0;
}

//-- IRQ handlers --------------------------------------------------------------
void SysTick_Handler()
{
    //Heartbit
    BSP_LED_Toggle(LED0_MSK);
}

void RTC_IRQHandler()
{
    print_time();
}
