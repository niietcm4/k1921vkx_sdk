/*==============================================================================
 * Тест режима разборка-сборка (память) DMA.
 * Пересылка 32-битных значений из массива в массив по цепочке по 0 каналу.
 * Создаются 3 альтернативные конфигурации данных для осуществления пересылок
 * по цепочке:
 *      1) массив 0 -> массив 1
 *      2) массив 1 -> массив 2
 *      3) массив 2 -> массив 3.
 * Запускается процесс пересылки, затем проверяется соответсвие данных массива 0
 * данным массива 3.
 *------------------------------------------------------------------------------
 * НИИЭТ, Богдан Колбов <kolbov@niiet.ru>
 *==============================================================================
 * ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО
 * ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ ГАРАНТИИ ТОВАРНОЙ
 * ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ НАЗНАЧЕНИЮ И ОТСУТСТВИЯ
 * НАРУШЕНИЙ, НО НЕ ОГРАНИЧИВАЯСЬ ИМИ. ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ
 * ПРЕДНАЗНАЧЕНО ДЛЯ ОЗНАКОМИТЕЛЬНЫХ ЦЕЛЕЙ И НАПРАВЛЕНО ТОЛЬКО НА
 * ПРЕДОСТАВЛЕНИЕ ДОПОЛНИТЕЛЬНОЙ ИНФОРМАЦИИ О ПРОДУКТЕ, С ЦЕЛЬЮ СОХРАНИТЬ ВРЕМЯ
 * ПОТРЕБИТЕЛЮ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ
 * ОТВЕТСТВЕННОСТИ ПО КАКИМ-ЛИБО ИСКАМ, ЗА ПРЯМОЙ ИЛИ КОСВЕННЫЙ УЩЕРБ, ИЛИ
 * ПО ИНЫМ ТРЕБОВАНИЯМ, ВОЗНИКШИМ ИЗ-ЗА ИСПОЛЬЗОВАНИЯ ПРОГРАММНОГО ОБЕСПЕЧЕНИЯ
 * ИЛИ ИНЫХ ДЕЙСТВИЙ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.
 *
 *                              2018 АО "НИИЭТ"
 *==============================================================================
 */

//-- Includes ------------------------------------------------------------------
#include "plib035.h"
#include "retarget_conf.h"

//-- Defines -------------------------------------------------------------------
#define TRANSFERS_TOTAL 32

//-- Variables -----------------------------------------------------------------
DMA_CtrlData_TypeDef DMA_CTRLDATA __ALIGNED(512);
uint32_t array0[TRANSFERS_TOTAL];
uint32_t array1[TRANSFERS_TOTAL];
uint32_t array2[TRANSFERS_TOTAL];
uint32_t array3[TRANSFERS_TOTAL];
volatile uint32_t irq_status = 0;
DMA_Channel_TypeDef Task[3]; /* 3 набора альтернативных управляющих структур */

//-- Peripheral init functions -------------------------------------------------
void dma_init()
{
    /* Базовый указатель */
    DMA_BasePtrConfig((uint32_t)(&DMA_CTRLDATA));

    /* Инициализация первичной структуры канала */
    DMA_ChannelInit_TypeDef DMA_ChannelInitStruct;
    DMA_ChannelStructInit(&DMA_ChannelInitStruct);
    DMA_ChannelInitStruct.SrcDataSize = DMA_DataSize_32;
    DMA_ChannelInitStruct.SrcDataInc = DMA_DataInc_32;
    DMA_ChannelInitStruct.DstDataSize = DMA_DataSize_32;
    DMA_ChannelInitStruct.DstDataInc = DMA_DataInc_32;
    DMA_ChannelInitStruct.ArbitrationRate = DMA_ArbitrationRate_4;
    DMA_ChannelInitStruct.TransfersTotal = 12; /* т.к. задач 3 -> 3*4=12 */
    DMA_ChannelInitStruct.Mode = DMA_Mode_PrmMemScatGath;
    DMA_ChannelInitStruct.SrcDataEndPtr = (uint32_t*)&(Task[2].RESERVED);
    DMA_ChannelInitStruct.DstDataEndPtr = (uint32_t*)&(DMA_CTRLDATA.ALT_DATA.CH[0].RESERVED);
    DMA_ChannelInit(&DMA_CTRLDATA.PRM_DATA.CH[0], &DMA_ChannelInitStruct);
    DMA_CTRLDATA.PRM_DATA.CH[1].SRC_DATA_END_PTR = 0xDEADDEAD;
    DMA_CTRLDATA.PRM_DATA.CH[1].DST_DATA_END_PTR = 0x66666666;
    DMA_CTRLDATA.ALT_DATA.CH[1].SRC_DATA_END_PTR = 0xCAFECAFE;
    DMA_CTRLDATA.PRM_DATA.CH[1].DST_DATA_END_PTR = 0x77777777;

    /* Инициализация контроллера */
    DMA_Init_TypeDef DMA_InitStruct;
    DMA_StructInit(&DMA_InitStruct);
    DMA_InitStruct.Channel = DMA_Channel_0;
    DMA_InitStruct.ChannelEnable = ENABLE;
    DMA_Init(&DMA_InitStruct);
    DMA_MasterEnableCmd(ENABLE);

    /* Инициализация альтернативных структур */
    DMA_ChannelInitStruct.ArbitrationRate = DMA_ArbitrationRate_32;
    DMA_ChannelInitStruct.TransfersTotal = TRANSFERS_TOTAL;
    DMA_ChannelInitStruct.Mode = DMA_Mode_AltMemScatGath;
    DMA_ChannelInitStruct.SrcDataEndPtr = &array0[TRANSFERS_TOTAL - 1];
    DMA_ChannelInitStruct.DstDataEndPtr = &array1[TRANSFERS_TOTAL - 1];
    DMA_ChannelInit(&Task[0], &DMA_ChannelInitStruct);
    DMA_ChannelInitStruct.SrcDataEndPtr = &array1[TRANSFERS_TOTAL - 1];
    DMA_ChannelInitStruct.DstDataEndPtr = &array2[TRANSFERS_TOTAL - 1];
    DMA_ChannelInit(&Task[1], &DMA_ChannelInitStruct);
    DMA_ChannelInitStruct.Mode = DMA_Mode_Basic;
    DMA_ChannelInitStruct.SrcDataEndPtr = &array2[TRANSFERS_TOTAL - 1];
    DMA_ChannelInitStruct.DstDataEndPtr = &array3[TRANSFERS_TOTAL - 1];
    DMA_ChannelInit(&Task[2], &DMA_ChannelInitStruct);

    /* NVIC init */
    NVIC_EnableIRQ(DMA_CH0_IRQn);
}
void periph_init()
{
    SystemCoreClockUpdate();
    retarget_init();
    dma_init();
    printf("\nAll peripherals inited, SYSCLK = %3d MHz\n", (int)(SystemCoreClock / 1E6));
}

//-- Main ----------------------------------------------------------------------
int main()
{
    periph_init();

    /* инициализация массивов */
    for (uint32_t i = 0; i < TRANSFERS_TOTAL; i++) {
        array0[i] = 0x10000001;
        array1[i] = 0x11011011;
        array2[i] = 0x12022021;
        array3[i] = 0x13033031;
    }

    /* начинаем пересылку */
    DMA_SwRequestCmd(DMA_Channel_0);

    /* ждем пока dma выполняет запросы */
    while (irq_status != DMA_Channel_0) {
    };

    /* проверяем результаты */
    uint32_t check_error = 0;
    for (uint32_t i = 0; i < TRANSFERS_TOTAL; i++) {
        printf("array3[%d]=0x%08x; array2[%d]=0x%08x; array1[%d]=0x%08x; array0[%d]=0x%08x\n",
               (int)i,
               (unsigned int)array3[i],
               (int)i,
               (unsigned int)array2[i],
               (int)i,
               (unsigned int)array1[i],
               (int)i,
               (unsigned int)array0[i]);
        if (array0[i] != array3[i])
            check_error++;
    }

    printf("All transfers done with %d errors!\n", (int)check_error);

    while (1) {
    };

    return 0;
}

//-- IRQ handlers --------------------------------------------------------------
void DMA_CH0_IRQHandler()
{
    irq_status |= DMA_Channel_0;
}

//-- Assert --------------------------------------------------------------------
#if defined USE_FULL_ASSERT
void assert_failed(uint8_t* file, uint32_t line)
{
    printf("Assert failed: file %s on line %d\n", file, (int)line);
    while (1) {
    };
}
#endif /* USE_FULL_ASSERT */
