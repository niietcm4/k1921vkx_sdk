/*==============================================================================
 * Опрос всех каналов АЦП с усреднением по каждому из них
 *------------------------------------------------------------------------------
 * НИИЭТ, Богдан Колбов <kolbov@niiet.ru>
 *==============================================================================
 * ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО
 * ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ ГАРАНТИИ ТОВАРНОЙ
 * ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ НАЗНАЧЕНИЮ И ОТСУТСТВИЯ
 * НАРУШЕНИЙ, НО НЕ ОГРАНИЧИВАЯСЬ ИМИ. ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ
 * ПРЕДНАЗНАЧЕНО ДЛЯ ОЗНАКОМИТЕЛЬНЫХ ЦЕЛЕЙ И НАПРАВЛЕНО ТОЛЬКО НА
 * ПРЕДОСТАВЛЕНИЕ ДОПОЛНИТЕЛЬНОЙ ИНФОРМАЦИИ О ПРОДУКТЕ, С ЦЕЛЬЮ СОХРАНИТЬ ВРЕМЯ
 * ПОТРЕБИТЕЛЮ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ
 * ОТВЕТСТВЕННОСТИ ПО КАКИМ-ЛИБО ИСКАМ, ЗА ПРЯМОЙ ИЛИ КОСВЕННЫЙ УЩЕРБ, ИЛИ
 * ПО ИНЫМ ТРЕБОВАНИЯМ, ВОЗНИКШИМ ИЗ-ЗА ИСПОЛЬЗОВАНИЯ ПРОГРАММНОГО ОБЕСПЕЧЕНИЯ
 * ИЛИ ИНЫХ ДЕЙСТВИЙ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.
 *
 *                              2018 АО "НИИЭТ"
 *==============================================================================
 */

//-- Includes ------------------------------------------------------------------
#include "K1921VK028.h"
#include "bsp.h"
#include "retarget_conf.h"

//-- Defines -------------------------------------------------------------------

//-- Peripheral init functions -------------------------------------------------
void adc_init()
{
    PMU->ADCPC = PMU_ADCPC_LDOEN0_Msk | PMU_ADCPC_LDOEN1_Msk |
                 PMU_ADCPC_LDOEN2_Msk | PMU_ADCPC_LDOEN3_Msk;
    //ждем готовности LDO в АЦП
    while (!(PMU->ADCPC_bit.LDORDY0 &&
             PMU->ADCPC_bit.LDORDY1 &&
             PMU->ADCPC_bit.LDORDY2 &&
             PMU->ADCPC_bit.LDORDY3)) {
    };

    //Инициализация ADC
    RCU->ADCCFG_bit.CLKSEL = RCU_ADCCFG_CLKSEL_PLLCLK;
    RCU->ADCCFG_bit.DIVN = 0x2; // div6
    RCU->ADCCFG_bit.DIVEN = 0x1;
    RCU->ADCCFG_bit.CLKEN = 0x1;
    RCU->ADCCFG_bit.RSTDIS = 0x1;

    //Настройка модуля АЦП
    //12бит и калибровка при включении
    ADC->ACTL[0].ACTL_bit.SELRES = ADC_ACTL_ACTL_SELRES_12bit;
    ADC->ACTL[0].ACTL_bit.CALEN = 1;
    ADC->ACTL[0].ACTL_bit.ADCEN = 1;
    ADC->ACTL[1].ACTL_bit.SELRES = ADC_ACTL_ACTL_SELRES_12bit;
    ADC->ACTL[1].ACTL_bit.CALEN = 1;
    ADC->ACTL[1].ACTL_bit.ADCEN = 1;
    ADC->ACTL[2].ACTL_bit.SELRES = ADC_ACTL_ACTL_SELRES_12bit;
    ADC->ACTL[2].ACTL_bit.CALEN = 1;
    ADC->ACTL[2].ACTL_bit.ADCEN = 1;
    ADC->ACTL[3].ACTL_bit.SELRES = ADC_ACTL_ACTL_SELRES_12bit;
    ADC->ACTL[3].ACTL_bit.CALEN = 1;
    ADC->ACTL[3].ACTL_bit.ADCEN = 1;

    //Настройка секвенсора 0: CH0 - CH11
    ADC->EMUX_bit.EM0 = ADC_EMUX_EM0_SwReq;
    ADC->SEQ[0].SCCTL_bit.ICNT = 0;
    ADC->SEQ[0].SCCTL_bit.RCNT = 0;
    ADC->SEQ[0].SRTMR = 0x0;
    ADC->SEQ[0].SRQCTL_bit.QAVGVAL = ADC_SEQ_SRQCTL_QAVGVAL_Average8;
    ADC->SEQ[0].SRQCTL_bit.QAVGEN = 0;
    ADC->SEQ[0].SRQCTL_bit.RQMAX = 11;
    ADC->SEQ[0].SRQSEL0_bit.RQ0 = 0;
    ADC->SEQ[0].SRQSEL0_bit.RQ1 = 1;
    ADC->SEQ[0].SRQSEL0_bit.RQ2 = 2;
    ADC->SEQ[0].SRQSEL0_bit.RQ3 = 3;
    ADC->SEQ[0].SRQSEL1_bit.RQ4 = 4;
    ADC->SEQ[0].SRQSEL1_bit.RQ5 = 5;
    ADC->SEQ[0].SRQSEL1_bit.RQ6 = 6;
    ADC->SEQ[0].SRQSEL1_bit.RQ7 = 7;
    ADC->SEQ[0].SRQSEL2_bit.RQ8 = 8;
    ADC->SEQ[0].SRQSEL2_bit.RQ9 = 9;
    ADC->SEQ[0].SRQSEL2_bit.RQ10 = 10;
    ADC->SEQ[0].SRQSEL2_bit.RQ11 = 11;

    //Настройка секвенсора 1: CH12 - CH23
    ADC->EMUX_bit.EM1 = ADC_EMUX_EM1_SwReq;
    ADC->SEQ[1].SCCTL_bit.ICNT = 0;
    ADC->SEQ[1].SCCTL_bit.RCNT = 0;
    ADC->SEQ[1].SRTMR = 0x0;
    ADC->SEQ[1].SRQCTL_bit.QAVGVAL = ADC_SEQ_SRQCTL_QAVGVAL_Average8;
    ADC->SEQ[1].SRQCTL_bit.QAVGEN = 0;
    ADC->SEQ[1].SRQCTL_bit.RQMAX = 11;
    ADC->SEQ[1].SRQSEL0_bit.RQ0 = 12;
    ADC->SEQ[1].SRQSEL0_bit.RQ1 = 13;
    ADC->SEQ[1].SRQSEL0_bit.RQ2 = 14;
    ADC->SEQ[1].SRQSEL0_bit.RQ3 = 15;
    ADC->SEQ[1].SRQSEL1_bit.RQ4 = 16;
    ADC->SEQ[1].SRQSEL1_bit.RQ5 = 17;
    ADC->SEQ[1].SRQSEL1_bit.RQ6 = 18;
    ADC->SEQ[1].SRQSEL1_bit.RQ7 = 19;
    ADC->SEQ[1].SRQSEL2_bit.RQ8 = 20;
    ADC->SEQ[1].SRQSEL2_bit.RQ9 = 21;
    ADC->SEQ[1].SRQSEL2_bit.RQ10 = 22;
    ADC->SEQ[1].SRQSEL2_bit.RQ11 = 23;

    //Настройка секвенсора 2: CH24 - CH35
    ADC->EMUX_bit.EM2 = ADC_EMUX_EM2_SwReq;
    ADC->SEQ[2].SCCTL_bit.ICNT = 0;
    ADC->SEQ[2].SCCTL_bit.RCNT = 0;
    ADC->SEQ[2].SRTMR = 0x0;
    ADC->SEQ[2].SRQCTL_bit.QAVGVAL = ADC_SEQ_SRQCTL_QAVGVAL_Average8;
    ADC->SEQ[2].SRQCTL_bit.QAVGEN = 0;
    ADC->SEQ[2].SRQCTL_bit.RQMAX = 11;
    ADC->SEQ[2].SRQSEL0_bit.RQ0 = 24;
    ADC->SEQ[2].SRQSEL0_bit.RQ1 = 25;
    ADC->SEQ[2].SRQSEL0_bit.RQ2 = 26;
    ADC->SEQ[2].SRQSEL0_bit.RQ3 = 27;
    ADC->SEQ[2].SRQSEL1_bit.RQ4 = 28;
    ADC->SEQ[2].SRQSEL1_bit.RQ5 = 29;
    ADC->SEQ[2].SRQSEL1_bit.RQ6 = 30;
    ADC->SEQ[2].SRQSEL1_bit.RQ7 = 31;
    ADC->SEQ[2].SRQSEL2_bit.RQ8 = 32;
    ADC->SEQ[2].SRQSEL2_bit.RQ9 = 33;
    ADC->SEQ[2].SRQSEL2_bit.RQ10 = 34;
    ADC->SEQ[2].SRQSEL2_bit.RQ11 = 35;

    //Настройка секвенсора 3: CH36 - CH47
    ADC->EMUX_bit.EM3 = ADC_EMUX_EM3_SwReq;
    ADC->SEQ[3].SCCTL_bit.ICNT = 11;
    ADC->SEQ[3].SCCTL_bit.RCNT = 0;
    ADC->SEQ[3].SRTMR = 0x0;
    ADC->SEQ[3].SRQCTL_bit.QAVGVAL = ADC_SEQ_SRQCTL_QAVGVAL_Average8;
    ADC->SEQ[3].SRQCTL_bit.QAVGEN = 0;
    ADC->SEQ[3].SRQCTL_bit.RQMAX = 11;
    ADC->SEQ[3].SRQSEL0_bit.RQ0 = 36;
    ADC->SEQ[3].SRQSEL0_bit.RQ1 = 37;
    ADC->SEQ[3].SRQSEL0_bit.RQ2 = 38;
    ADC->SEQ[3].SRQSEL0_bit.RQ3 = 39;
    ADC->SEQ[3].SRQSEL1_bit.RQ4 = 40;
    ADC->SEQ[3].SRQSEL1_bit.RQ5 = 41;
    ADC->SEQ[3].SRQSEL1_bit.RQ6 = 42;
    ADC->SEQ[3].SRQSEL1_bit.RQ7 = 43;
    ADC->SEQ[3].SRQSEL2_bit.RQ8 = 44;
    ADC->SEQ[3].SRQSEL2_bit.RQ9 = 45;
    ADC->SEQ[3].SRQSEL2_bit.RQ10 = 46;
    ADC->SEQ[3].SRQSEL2_bit.RQ11 = 47;

    //Включаем секвенсоры
    ADC->SEQSYNC = ADC_SEQSYNC_SYNC0_Msk | ADC_SEQSYNC_SYNC1_Msk |
                   ADC_SEQSYNC_SYNC2_Msk | ADC_SEQSYNC_SYNC3_Msk;
    ADC->SEQEN = ADC_SEQEN_SEQEN0_Msk | ADC_SEQEN_SEQEN1_Msk |
                 ADC_SEQEN_SEQEN2_Msk | ADC_SEQEN_SEQEN3_Msk;

    //Ждем пока АЦП пройдут инициализацию, начатую в самом начале
    while (!(ADC->ACTL[0].ACTL_bit.ADCRDY &
             ADC->ACTL[1].ACTL_bit.ADCRDY &
             ADC->ACTL[2].ACTL_bit.ADCRDY &
             ADC->ACTL[3].ACTL_bit.ADCRDY)) {
    };
}

void periph_init()
{
    SystemCoreClockUpdate();
    retarget_init();
    BSP_LED_Init();
    BSP_Btn_Init();
    adc_init();
    printf("\nAll peripherals inited, SYSCLK = %3d MHz\n", (int)(SystemCoreClock / 1E6));
}

//-- Main ----------------------------------------------------------------------

int main()
{
    periph_init();
    SysTick_Config(10000000);

    printf("Press SB3 button to perform ADC 0-47 channels conversion\n");

    while (1) {
        if (BSP_Btn_GetState() == BSP_Btn_State_SB3) {
            ADC->SEQSYNC_bit.GSYNC = 1;

            while (!(ADC->RIS_bit.SEQRIS0 &
                     ADC->RIS_bit.SEQRIS1 &
                     ADC->RIS_bit.SEQRIS2 &
                     ADC->RIS_bit.SEQRIS3)) {
            };

            printf("\n=====================\n");

            for (int seqn = 0; seqn < 4; seqn++) {
                printf("\n");
                for (int chn = 0; chn < 12; chn++) {
                    int ch_res = ADC->SEQ[seqn].SFIFO;
                    printf("CH%02d - 0x%03x - %.04fV\n",
                           seqn * 12 + chn, ch_res, (ch_res * 3.300f) / 4096.0f);
                }
            }
            ADC->IC = ADC_IC_SEQIC0_Msk | ADC_IC_SEQIC1_Msk |
                      ADC_IC_SEQIC2_Msk | ADC_IC_SEQIC3_Msk;
        }
    }
}

//-- IRQ handlers --------------------------------------------------------------
void SysTick_Handler()
{
    //Heartbit
    BSP_LED_Toggle(LED0_MSK);
}
