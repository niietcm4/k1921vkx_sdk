/*==============================================================================
 * Функции для работы с EEPROM AT93C46 через SPI микроконтроллера К1921ВК028.
 * Простая реализация с использованием программных задержек ожидания выполнения
 * операции. CS управляется вручную, как GPIO.
 *------------------------------------------------------------------------------
 * НИИЭТ, Богдан Колбов <kolbov@niiet.ru>
 *==============================================================================
 * ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО
 * ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ ГАРАНТИИ ТОВАРНОЙ
 * ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ НАЗНАЧЕНИЮ И ОТСУТСТВИЯ
 * НАРУШЕНИЙ, НО НЕ ОГРАНИЧИВАЯСЬ ИМИ. ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ
 * ПРЕДНАЗНАЧЕНО ДЛЯ ОЗНАКОМИТЕЛЬНЫХ ЦЕЛЕЙ И НАПРАВЛЕНО ТОЛЬКО НА
 * ПРЕДОСТАВЛЕНИЕ ДОПОЛНИТЕЛЬНОЙ ИНФОРМАЦИИ О ПРОДУКТЕ, С ЦЕЛЬЮ СОХРАНИТЬ ВРЕМЯ
 * ПОТРЕБИТЕЛЮ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ
 * ОТВЕТСТВЕННОСТИ ПО КАКИМ-ЛИБО ИСКАМ, ЗА ПРЯМОЙ ИЛИ КОСВЕННЫЙ УЩЕРБ, ИЛИ
 * ПО ИНЫМ ТРЕБОВАНИЯМ, ВОЗНИКШИМ ИЗ-ЗА ИСПОЛЬЗОВАНИЯ ПРОГРАММНОГО ОБЕСПЕЧЕНИЯ
 * ИЛИ ИНЫХ ДЕЙСТВИЙ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.
 *
 *                              2018 АО "НИИЭТ"
 *==============================================================================
 */

//-- Includes ------------------------------------------------------------------
#include "at93c46.h"

//-- Private Defines -----------------------------------------------------------
#define AT93C46_OPCODE_READ ((uint16_t)0x0180)
#define AT93C46_OPCODE_WEN ((uint16_t)0x9800)
#define AT93C46_OPCODE_WRITE ((uint16_t)0x0140)
#define AT93C46_OPCODE_WRALL ((uint16_t)0x0110)
#define AT93C46_OPCODE_WDS ((uint16_t)0x0100)
#define AT93C46_OPCODE_ERASE ((uint16_t)0x01C0)
#define AT93C46_OPCODE_ERAL ((uint16_t)0x0120)

//-- Private Types -------------------------------------------------------------

//-- Private Functions ---------------------------------------------------------
__STATIC_INLINE void SetCS(void)
{
    GPIO_SetBits(GPIOA, GPIO_Pin_3);
}

__STATIC_INLINE void ClrCS()
{
    GPIO_ClearBits(GPIOA, GPIO_Pin_3);
}

static void EnableDelay(void)
{
    CoreDebug->DEMCR = CoreDebug_DEMCR_TRCENA_Msk;
    DWT->CYCCNT = 0;
    DWT->CTRL = DWT_CTRL_CYCCNTENA_Msk;
}

//Not for precision delays
static void Delay(uint32_t us)
{
    uint32_t ticks = us * (SystemCoreClock / 1000000);
    DWT->CYCCNT = 0;
    while (DWT->CYCCNT < ticks) {
    };
}

//-- Functions -----------------------------------------------------------------
void AT93C46_Init()
{
    SPI_Init_TypeDef SPI_InitStruct;
    GPIO_Init_TypeDef GPIO_InitStruct;

    RCU_SPIClkConfig(SPI0_Num, RCU_PeriphClk_PLLClk, 0, DISABLE);
    RCU_SPIClkCmd(SPI0_Num, ENABLE);
    RCU_SPIRstCmd(SPI0_Num, ENABLE);
    SPI_StructInit(&SPI_InitStruct);
    SPI_InitStruct.SCKDiv = 9;
    SPI_InitStruct.SCKDivExtra = 50;
    SPI_InitStruct.FrameFormat = SPI_FrameFormat_SPI;
    SPI_InitStruct.DataWidth = SPI_DataWidth_16;
    SPI_Init(SPI0, &SPI_InitStruct);
    SPI_Cmd(SPI0, ENABLE);

    RCU_AHBClkCmd(RCU_AHBClk_GPIOA, ENABLE);
    RCU_AHBRstCmd(RCU_AHBRst_GPIOA, ENABLE);
    GPIO_StructInit(&GPIO_InitStruct);
    GPIO_InitStruct.Pin = GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2;
    GPIO_InitStruct.AltFuncNum = GPIO_AltFuncNum_1;
    GPIO_InitStruct.AltFunc = ENABLE;
    GPIO_InitStruct.Digital = ENABLE;
    GPIO_Init(GPIOA, &GPIO_InitStruct);
    GPIO_InitStruct.Pin = GPIO_Pin_3;
    GPIO_InitStruct.Out = ENABLE;
    GPIO_InitStruct.AltFuncNum = GPIO_AltFuncNum_None;
    GPIO_InitStruct.AltFunc = DISABLE;
    GPIO_Init(GPIOA, &GPIO_InitStruct);

    EnableDelay();
}

void AT93C46_WriteEnable(void)
{
    SetCS();
    SPI_SendData(SPI0, AT93C46_OPCODE_WEN);
    while (SPI_FlagStatus(SPI0, SPI_Flag_Busy)) {
    };
    ClrCS();
    SPI_RecieveData(SPI0); //dummy read
    Delay(1);              //min 1us delay
}

void AT93C46_WriteDisable(void)
{
    SetCS();
    SPI_SendData(SPI0, AT93C46_OPCODE_WDS);
    while (SPI_FlagStatus(SPI0, SPI_Flag_Busy)) {
    };
    ClrCS();
    SPI_RecieveData(SPI0); //dummy read
    Delay(1);              //min 1us delay
}

uint32_t AT93C46_Write(uint16_t Addr, uint16_t Data)
{
    if (Addr < AT93C46_WORDS_TOTAL) {
        SetCS();
        SPI_SendData(SPI0, AT93C46_OPCODE_WRITE | Addr);
        SPI_SendData(SPI0, Data);
        while (SPI_FlagStatus(SPI0, SPI_Flag_Busy)) {
        };
        ClrCS();
        SPI_RecieveData(SPI0); //dummy read
        SPI_RecieveData(SPI0); //dummy read
        Delay(15000);          //min 15ms delay
        return 0;
    } else
        return 1;
}

void AT93C46_WriteAll(uint16_t Data)
{
    SetCS();
    SPI_SendData(SPI0, AT93C46_OPCODE_WRALL);
    SPI_SendData(SPI0, Data);
    while (SPI_FlagStatus(SPI0, SPI_Flag_Busy)) {
    };
    ClrCS();
    SPI_RecieveData(SPI0); //dummy read
    SPI_RecieveData(SPI0); //dummy read
    Delay(15000);          //min 15ms delay
}

uint16_t AT93C46_Read(uint16_t Addr)
{
    uint16_t buf = 0;
    if (Addr < AT93C46_WORDS_TOTAL) {
        SetCS();
        SPI_SendData(SPI0, AT93C46_OPCODE_READ | Addr);
        SPI_SendData(SPI0, 0x0000);
        SPI_SendData(SPI0, 0x0000);
        while (SPI_FlagStatus(SPI0, SPI_Flag_Busy)) {
        };
        ClrCS();
        SPI_RecieveData(SPI0); //dummy read
        buf = SPI_RecieveData(SPI0);
        buf = (buf << 1) | (SPI_RecieveData(SPI0) >> 15);
        Delay(1); //min 1us delay
    }
    return buf;
}

uint32_t AT93C46_Erase(uint16_t Addr)
{
    if (Addr < AT93C46_WORDS_TOTAL) {
        SetCS();
        SPI_SendData(SPI0, AT93C46_OPCODE_ERASE | Addr);
        while (SPI_FlagStatus(SPI0, SPI_Flag_Busy)) {
        };
        ClrCS();
        SPI_RecieveData(SPI0); //dummy read
        Delay(15000);          //min 15ms delay
        return 0;
    } else
        return 1;
}

void AT93C46_EraseAll(void)
{
    SetCS();
    SPI_SendData(SPI0, AT93C46_OPCODE_ERAL);
    while (SPI_FlagStatus(SPI0, SPI_Flag_Busy)) {
    };
    ClrCS();
    SPI_RecieveData(SPI0); //dummy read
    Delay(15000);          //min 15ms delay
}
