/*==============================================================================
 * Обсчет вращения 3D фигуры с 4 вершинами
 *------------------------------------------------------------------------------
 * НИИЭТ, Богдан Колбов <kolbov@niiet.ru>
 *==============================================================================
 * ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО
 * ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ ГАРАНТИИ ТОВАРНОЙ
 * ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ НАЗНАЧЕНИЮ И ОТСУТСТВИЯ
 * НАРУШЕНИЙ, НО НЕ ОГРАНИЧИВАЯСЬ ИМИ. ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ
 * ПРЕДНАЗНАЧЕНО ДЛЯ ОЗНАКОМИТЕЛЬНЫХ ЦЕЛЕЙ И НАПРАВЛЕНО ТОЛЬКО НА
 * ПРЕДОСТАВЛЕНИЕ ДОПОЛНИТЕЛЬНОЙ ИНФОРМАЦИИ О ПРОДУКТЕ, С ЦЕЛЬЮ СОХРАНИТЬ ВРЕМЯ
 * ПОТРЕБИТЕЛЮ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ
 * ОТВЕТСТВЕННОСТИ ПО КАКИМ-ЛИБО ИСКАМ, ЗА ПРЯМОЙ ИЛИ КОСВЕННЫЙ УЩЕРБ, ИЛИ
 * ПО ИНЫМ ТРЕБОВАНИЯМ, ВОЗНИКШИМ ИЗ-ЗА ИСПОЛЬЗОВАНИЯ ПРОГРАММНОГО ОБЕСПЕЧЕНИЯ
 * ИЛИ ИНЫХ ДЕЙСТВИЙ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.
 *
 *                              2018 АО "НИИЭТ"
 *==============================================================================
 */

//-- Includes ------------------------------------------------------------------
#include "K1921VK028.h"
#include "bsp.h"
#include "math.h"
#include "retarget_conf.h"

//-- Defines -------------------------------------------------------------------
#define VERTEX_NUM 4
#define DIM_NUM 3
enum {
    AXE_X,
    AXE_Y,
    AXE_Z
};
#define ITERATIONS 100

#define FP_AS_UINT32(VAR) (*(uint32_t*)&(VAR))
#define UINT32_AS_FP(VAR) (*(float*)&(VAR))

//-- Types ---------------------------------------------------------------------

//-- Peripheral init functions -------------------------------------------------
void tmr_init()
{
    RCU->PCLKCFG0_bit.TMR0EN = 1;
    RCU->PRSTCFG0_bit.TMR0EN = 1;
    TMR0->LOAD = 0xFFFFFFFF;
}

void periph_init()
{
    SystemCoreClockUpdate();
    retarget_init();
    BSP_LED_Init();
    tmr_init();
    printf("\nAll peripherals inited, SYSCLK = %3d MHz\n", (int)(SystemCoreClock / 1E6));
}

//-- Main ----------------------------------------------------------------------
volatile float rmatrix[DIM_NUM][DIM_NUM] = { { 1.0f, 0.0f, 0.0f },
                                             { 0.0f, 1.0f, 0.0f },
                                             { 0.0f, 0.0f, 1.0f } };

volatile float figure[VERTEX_NUM][DIM_NUM] = { { 0.0f, 30.0f, -15.0f },
                                               { -26.0f, -15.0f, -15.0f },
                                               { 26.0f, -15.0f, -15.0f },
                                               { 0.0f, -15.0f, 30.0f } };

void fill_rmatrix_fpu(float a, float b, float c)
{
    float cos_a = cosf(a);
    float cos_b = cosf(b);
    float cos_c = cosf(c);
    float sin_a = sinf(a);
    float sin_b = sinf(b);
    float sin_c = sinf(c);

    rmatrix[0][0] = cos_b * cos_c;
    rmatrix[0][1] = -cos_b * sin_c;
    rmatrix[0][2] = -sin_b;
    rmatrix[1][0] = -sin_a * sin_b * cos_c + cos_a * sin_c;
    rmatrix[1][1] = sin_a * sin_b * sin_c + cos_a * cos_c;
    rmatrix[1][2] = -sin_a * cos_b;
    rmatrix[2][0] = cos_a * sin_b * cos_c + sin_a * sin_c;
    rmatrix[2][1] = -cos_a * sin_b * sin_c + sin_a * cos_c;
    rmatrix[2][2] = cos_a * cos_b;
}

void fill_rmatrix_tmu(float a, float b, float c)
{
    volatile float cos_a, cos_b, cos_c;
    volatile float sin_a, sin_b, sin_c;
    volatile uint32_t tmu_cmd = (TMU_CMD_FUNC_SinCos << TMU_CMD_FUNC_Pos) |
                                (0x1F << TMU_CMD_ARGT_Pos) |
                                TMU_CMD_WAITRD_Msk |
                                TMU_CMD_START_Msk;

    //norm to 0...2pi
    a -= (float)((int)(a / (float)(M_TWOPI)) * (float)M_TWOPI);
    b -= (float)((int)(b / (float)(M_TWOPI)) * (float)M_TWOPI);
    c -= (float)((int)(b / (float)(M_TWOPI)) * (float)M_TWOPI);

    TMU->PHIN = FP_AS_UINT32(a);
    TMU->CMD = tmu_cmd;
    cos_a = UINT32_AS_FP(TMU->XOUT);
    sin_a = UINT32_AS_FP(TMU->YOUT);

    TMU->PHIN = FP_AS_UINT32(b);
    TMU->CMD = tmu_cmd;
    cos_b = UINT32_AS_FP(TMU->XOUT);
    sin_b = UINT32_AS_FP(TMU->YOUT);

    TMU->PHIN = FP_AS_UINT32(c);
    TMU->CMD = tmu_cmd;
    cos_c = UINT32_AS_FP(TMU->XOUT);
    sin_c = UINT32_AS_FP(TMU->YOUT);

    rmatrix[0][0] = cos_b * cos_c;
    rmatrix[0][1] = -cos_b * sin_c;
    rmatrix[0][2] = -sin_b;
    rmatrix[1][0] = -sin_a * sin_b * cos_c + cos_a * sin_c;
    rmatrix[1][1] = sin_a * sin_b * sin_c + cos_a * cos_c;
    rmatrix[1][2] = -sin_a * cos_b;
    rmatrix[2][0] = cos_a * sin_b * cos_c + sin_a * sin_c;
    rmatrix[2][1] = -cos_a * sin_b * sin_c + sin_a * cos_c;
    rmatrix[2][2] = cos_a * cos_b;
}

void rotate_figure(void)
{
    float temp[3];

    for (int i = 0; i < VERTEX_NUM; i++) {
        for (int j = 0; j < DIM_NUM; j++)
            temp[j] = figure[i][j];

        for (int j = 0; j < DIM_NUM; j++) {
            figure[i][j] = 0;
            for (int r = 0; r < DIM_NUM; r++) {
                figure[i][j] = figure[i][j] + temp[r] * rmatrix[r][j];
            }
        }
    }
}

float check_edge(void)
{
    float temp;

    temp = (figure[1][AXE_X] - figure[2][AXE_X]) * (figure[1][AXE_X] - figure[2][AXE_X]);
    temp += (figure[1][AXE_Y] - figure[2][AXE_Y]) * (figure[1][AXE_Y] - figure[2][AXE_Y]);
    temp += (figure[1][AXE_Z] - figure[2][AXE_Z]) * (figure[1][AXE_Z] - figure[2][AXE_Z]);

    return sqrtf(temp);
}

int main()
{
    periph_init();
    printf("Rotate 3d figure, %d iterations:\n", ITERATIONS);

    uint32_t check_error;
    float a, b, c;

    check_error = 0;
    a = 0.0f;
    b = 0.0f;
    c = 0.0f;
    TMR0->VALUE = 0xFFFFFFFF;
    TMR0->CTRL_bit.ON = 1;
    for (volatile int i = 0; i < ITERATIONS; i++) {
        a += 0.017f, b += -0.02f, c += 0.014f;
        fill_rmatrix_fpu(a, b, c);
        rotate_figure();
        float res = check_edge();
        if ((res < 51.5f) && (res > 52.5f)) {
            check_error = 1;
        }
    }
    TMR0->CTRL_bit.ON = 0;
    printf("FPU: %d ticks/iteration, check: %s\n",
           (int)((0xFFFFFFFF - TMR0->VALUE) / ITERATIONS),
           check_error ? "ERROR" : "OK");

    check_error = 0;
    a = 0.0f;
    b = 0.0f;
    c = 0.0f;
    TMR0->VALUE = 0xFFFFFFFF;
    TMR0->CTRL_bit.ON = 1;
    for (volatile int i = 0; i < ITERATIONS; i++) {
        a += 0.017f, b += -0.02f, c += 0.014f;
        fill_rmatrix_tmu(a, b, c);
        rotate_figure();
        float res = check_edge();
        if ((res < 51.5f) && (res > 52.5f)) {
            check_error = 1;
        }
    }
    TMR0->CTRL_bit.ON = 0;
    printf("TMU: %d ticks/iteration, check: %s\n\n",
           (int)((0xFFFFFFFF - TMR0->VALUE) / ITERATIONS),
           check_error ? "ERROR" : "OK");

    SysTick_Config(10000000);
    while (1) {
    };
    return 0;
}

//-- IRQ handlers --------------------------------------------------------------
void SysTick_Handler()
{
    //Heartbit
    BSP_LED_Toggle(LED0_MSK);
}
