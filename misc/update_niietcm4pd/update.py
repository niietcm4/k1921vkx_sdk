#!/user/bin/env python3

"""
Script to update NIIETCM4 PD lib and example projects
"""

import sys
import shutil
import os
import glob
import re


def copytree(src, dst, symlinks=False, ignore=None):
    if not os.path.exists(dst):
        os.makedirs(dst)
    for item in os.listdir(src):
        s = os.path.join(src, item)
        d = os.path.join(dst, item)
        if os.path.isdir(s):
            copytree(s, d, symlinks, ignore)
        else:
            if not os.path.exists(d) or os.stat(s).st_mtime - os.stat(d).st_mtime > 1:
                shutil.copy2(s, d)


if sys.version_info < (3, 5, 0):
    print("You need python 3.5.0 or later to run this script!")
    exit(1)


# -- niietcm4 pd lib --
copytree('../../../niietcm4_pd/Libs/Periph_Driver/Include', '../../platform/niietcm4_pd/inc')
copytree('../../../niietcm4_pd/Libs/Periph_Driver/Source', '../../platform/niietcm4_pd/src')
shutil.copy('../../../niietcm4_pd/README.md', '../../platform/niietcm4_pd/README.md')
shutil.copy('../../../niietcm4_pd/RELEASE.md', '../../platform/niietcm4_pd/RELEASE.md')
shutil.copy('../../../niietcm4_pd/Libs/Device/NIIET/K1921VK01T/Include/K1921VK01T.h', '../../platform/Device/NIIET/K1921VK01T/Include/K1921VK01T.h')

# -- niietcm4 pd example projects --
proj_qbs = glob.glob('../../../niietcm4_pd/Examples/K1921VK01T/**/*.qbs', recursive=True)
proj = []
for i in range(len(proj_qbs)):
    proj += [{"dir": os.path.dirname(proj_qbs[i]),
              "name": os.path.dirname(proj_qbs[i]).split('/')[-1],
              "group": os.path.dirname(proj_qbs[i]).split('/')[-3].lower()}]

for p in proj:
    dest_dir = '../../projects/niietcm4_pd/' + p["group"] + '/' + p["name"]

    # main
    if not os.path.exists(dest_dir + '/app'):
        os.makedirs(dest_dir + '/app')
    shutil.copy(p["dir"] + '/main.c', dest_dir + '/app/main.c')
    with open(dest_dir + '/app/main.c', 'r') as f:
        main_lines = f.readlines()
    retarget_h_re = re.compile('(^#include ")retarget(.h".*)')
    dma_config_re = re.compile('(^DMA_ConfigData_TypeDef DMA_CONFIGDATA.*)')
    for line_num in range(0, len(main_lines)):
        match = re.search(retarget_h_re, main_lines[line_num])
        if match:
            main_lines[line_num] = match.group(1) + 'retarget_conf' + match.group(2) + '\n'
        match = re.search(dma_config_re, main_lines[line_num])
        if match:
            main_lines[line_num] = '#if defined (__CMCPPARM__)\n#pragma data_alignment=1024\n#endif\nDMA_ConfigData_TypeDef DMA_CONFIGDATA __ALIGNED(1024);\n'
    with open(dest_dir + '/app/main.c', 'w') as f:
        for line in main_lines:
            f.write(line)

    # GCC
    if os.path.exists(dest_dir + '/GCC'):
        shutil.rmtree(dest_dir + '/GCC', ignore_errors=True)
    copytree('./GCC', dest_dir + '/GCC')
    with open(dest_dir + '/GCC/CMakeLists.txt', 'r') as f:
        gcc_lines = f.readlines()
    gcc_proj_re = re.compile('(^PROJECT\().*(\).*)')
    gcc_retarget_clk_re = re.compile('(^ADD_DEFINITIONS\(-DRETARGET_CLK_VAL=)OSECLK_VAL(\).*)')
    for line_num in range(0, len(gcc_lines)):
        match = re.search(gcc_proj_re, gcc_lines[line_num])
        if match:
            gcc_lines[line_num] = match.group(1) + p["name"] + match.group(2) + '\n'
        if p["group"] == 'adc':
            match = re.search(gcc_retarget_clk_re, gcc_lines[line_num])
            if match:
                gcc_lines[line_num] = match.group(1) + '72000000' + match.group(2) + '\n'
    with open(dest_dir + '/GCC/CMakeLists.txt', 'w') as f:
        for line in gcc_lines:
            f.write(line)

    # ARM
    if os.path.exists(dest_dir + '/ARM'):
        shutil.rmtree(dest_dir + '/ARM', ignore_errors=True)
    copytree('./ARM', dest_dir + '/ARM')
    with open(dest_dir + '/ARM/template.uvprojx', 'r') as f:
        arm_lines = f.readlines()
    arm_target_re = re.compile('(^.*<TargetName>).*(</TargetName>)')
    arm_output_re = re.compile('(^.*<OutputName>).*(</OutputName>)')
    arm_retarget_clk_re = re.compile('(^.*<Define>.*RETARGET_CLK_VAL=)OSECLK_VAL(.*)')
    for line_num in range(0, len(arm_lines)):
        match = re.search(arm_target_re, arm_lines[line_num])
        if match:
            arm_lines[line_num] = match.group(1) + p["name"] + match.group(2) + '\n'
        match = re.search(arm_output_re, arm_lines[line_num])
        if match:
            arm_lines[line_num] = match.group(1) + p["name"] + match.group(2) + '\n'
        if p["group"] == 'adc':
            match = re.search(arm_retarget_clk_re, arm_lines[line_num])
            if match:
                arm_lines[line_num] = match.group(1) + '72000000' + match.group(2) + '\n'
    with open(dest_dir + '/ARM/template.uvprojx', 'w') as f:
        for line in arm_lines:
            f.write(line)
    os.rename(dest_dir + '/ARM/template.uvprojx', dest_dir + '/ARM/' + p["name"] + '.uvprojx')

    # IAR
    if os.path.exists(dest_dir + '/IAR'):
        shutil.rmtree(dest_dir + '/IAR', ignore_errors=True)
    copytree('./IAR', dest_dir + '/IAR')
    with open(dest_dir + '/IAR/template.ewp', 'r') as f:
        iar_p_lines = f.readlines()
    iar_retarget_clk_re = re.compile('(^.*<state>.*RETARGET_CLK_VAL=)OSECLK_VAL(.*)')
    for line_num in range(0, len(iar_p_lines)):
        if p["group"] == 'adc':
            match = re.search(iar_retarget_clk_re, iar_p_lines[line_num])
            if match:
                iar_p_lines[line_num] = match.group(1) + '72000000' + match.group(2) + '\n'
    with open(dest_dir + '/IAR/template.ewp', 'w') as f:
        for line in iar_p_lines:
            f.write(line)
    with open(dest_dir + '/IAR/template.eww', 'r') as f:
        iar_w_lines = f.readlines()
    iar_proj_re = re.compile('(^.*<path>\$WS_DIR\$\\\).*(\.ewp</path>)')
    for line_num in range(0, len(iar_w_lines)):
        match = re.search(iar_proj_re, iar_w_lines[line_num])
        if match:
            iar_w_lines[line_num] = match.group(1) + p["name"] + match.group(2) + '\n'
    with open(dest_dir + '/IAR/template.eww', 'w') as f:
        for line in iar_w_lines:
            f.write(line)
    os.rename(dest_dir + '/IAR/template.eww', dest_dir + '/IAR/' + p["name"] + '.eww')
    os.rename(dest_dir + '/IAR/template.ewp', dest_dir + '/IAR/' + p["name"] + '.ewp')
    os.rename(dest_dir + '/IAR/template.ewd', dest_dir + '/IAR/' + p["name"] + '.ewd')
