/**
  ******************************************************************************
  * @file    main.c
  *
  * @brief   Пример, использующий функции драйвера CAP, чтобы организовать
  *          измерение периода входного прямоугольного сигнала.
  *
  *          Компилятор: GCC ARM 4.9.3
  *          Среда: Qt Creator 3.4.2
  *
  * @author  НИИЭТ
  *             - Богдан Колбов (bkolbov), kolbov@niiet.ru
  * @date    21.03.2016
  *
  ******************************************************************************
  * @attention
  *
  * ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО
  * ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ ГАРАНТИИ ТОВАРНОЙ
  * ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ НАЗНАЧЕНИЮ И ОТСУТСТВИЯ
  * НАРУШЕНИЙ, НО НЕ ОГРАНИЧИВАЯСЬ ИМИ. ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ
  * ПРЕДНАЗНАЧЕНО ДЛЯ ОЗНАКОМИТЕЛЬНЫХ ЦЕЛЕЙ И НАПРАВЛЕНО ТОЛЬКО НА
  * ПРЕДОСТАВЛЕНИЕ ДОПОЛНИТЕЛЬНОЙ ИНФОРМАЦИИ О ПРОДУКТЕ, С ЦЕЛЬЮ СОХРАНИТЬ ВРЕМЯ
  * ПОТРЕБИТЕЛЮ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ
  * ОТВЕТСТВЕННОСТИ ПО КАКИМ-ЛИБО ИСКАМ, ЗА ПРЯМОЙ ИЛИ КОСВЕННЫЙ УЩЕРБ, ИЛИ
  * ПО ИНЫМ ТРЕБОВАНИЯМ, ВОЗНИКШИМ ИЗ-ЗА ИСПОЛЬЗОВАНИЯ ПРОГРАММНОГО ОБЕСПЕЧЕНИЯ
  * ИЛИ ИНЫХ ДЕЙСТВИЙ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.
  *
  * <h2><center>&copy; 2016 ОАО "НИИЭТ"</center></h2>
  ******************************************************************************
  */

#include "niietcm4.h"
#include "system_K1921VK01T.h"
#include "retarget_conf.h"

void SystemInit()
{}

void PeriphInit()
{
    //инициализация printf
    retarget_init();

    // инициализация блока захвата
    RCC_PeriphRstCmd(RCC_PeriphRst_CAP0, ENABLE);

    // выбираем CAP режим
    CAP_Init_TypeDef CAP_InitStruct;
    CAP_StructInit(&CAP_InitStruct);
    CAP_InitStruct.CAP_Mode = CAP_Mode_Capture;
    CAP_Init(NT_CAP0, &CAP_InitStruct);

    // настраиваем CAP режим
    // захват блоком CAP0
    CAP_Capture_Init_TypeDef CAP_Capture_InitStruct;
    CAP_Capture_StructInit(&CAP_Capture_InitStruct);
    CAP_Capture_InitStruct.CAP_CaptureMode = CAP_Capture_Mode_Cycle;
    CAP_Capture_InitStruct.CAP_Capture_PolarityEvent0 = CAP_Capture_Polarity_PosEdge;
    CAP_Capture_InitStruct.CAP_Capture_RstEvent0 = ENABLE;
    CAP_Capture_Init(NT_CAP0, &CAP_Capture_InitStruct);

    // включаем прерывание по событию
    CAP_ITCmd(NT_CAP0, CAP_ITSource_CapEvent0, ENABLE);
    NVIC_EnableIRQ(CAP0_IRQn);

    // инициализация выводов блоков CAP0
    GPIO_Init_TypeDef GPIO_InitStruct;
    GPIO_StructInit(&GPIO_InitStruct);
    GPIO_InitStruct.GPIO_Dir = GPIO_Dir_In;
    GPIO_InitStruct.GPIO_Out = GPIO_Out_Dis;
    GPIO_InitStruct.GPIO_AltFunc = GPIO_AltFunc_2;
    GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AltFunc;
    GPIO_InitStruct.GPIO_Pin = GPIO_Pin_4;
    GPIO_Init(NT_GPIOE, &GPIO_InitStruct);

}

int main()
{
    PeriphInit();


    printf("External signal period in system ticks:\n");
    CAP_TimerCmd(NT_CAP0, ENABLE);
    CAP_Capture_Cmd(NT_CAP0, ENABLE);

    while(1);
}

void CAP0_IRQHandler()
{
    CAP_ITStatusClear(NT_CAP0, CAP_ITSource_CapEvent0);
    CAP_ITStatusClear(NT_CAP0, CAP_ITSource_GeneralInt);
    CAP_ITPendClear(NT_CAP0);
    printf("%d\r", CAP_Capture_GetCap0(NT_CAP0));
}
