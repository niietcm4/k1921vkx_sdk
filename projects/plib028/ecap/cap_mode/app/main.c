/*==============================================================================
 * Пример, использующий функции драйвера ECAP, чтобы организовать измерение
 * частоты входного прямоугольного сигнала
 *------------------------------------------------------------------------------
 * НИИЭТ, Богдан Колбов <kolbov@niiet.ru>
 *==============================================================================
 * ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО
 * ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ ГАРАНТИИ ТОВАРНОЙ
 * ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ НАЗНАЧЕНИЮ И ОТСУТСТВИЯ
 * НАРУШЕНИЙ, НО НЕ ОГРАНИЧИВАЯСЬ ИМИ. ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ
 * ПРЕДНАЗНАЧЕНО ДЛЯ ОЗНАКОМИТЕЛЬНЫХ ЦЕЛЕЙ И НАПРАВЛЕНО ТОЛЬКО НА
 * ПРЕДОСТАВЛЕНИЕ ДОПОЛНИТЕЛЬНОЙ ИНФОРМАЦИИ О ПРОДУКТЕ, С ЦЕЛЬЮ СОХРАНИТЬ ВРЕМЯ
 * ПОТРЕБИТЕЛЮ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ
 * ОТВЕТСТВЕННОСТИ ПО КАКИМ-ЛИБО ИСКАМ, ЗА ПРЯМОЙ ИЛИ КОСВЕННЫЙ УЩЕРБ, ИЛИ
 * ПО ИНЫМ ТРЕБОВАНИЯМ, ВОЗНИКШИМ ИЗ-ЗА ИСПОЛЬЗОВАНИЯ ПРОГРАММНОГО ОБЕСПЕЧЕНИЯ
 * ИЛИ ИНЫХ ДЕЙСТВИЙ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.
 *
 *                              2019 АО "НИИЭТ"
 *==============================================================================
 */

//-- Includes ------------------------------------------------------------------
#include "plib028.h"
#include "retarget_conf.h"

//-- Defines -------------------------------------------------------------------

//-- Peripheral init functions -------------------------------------------------
void gpio_init()
{
    RCU_AHBClkCmd(RCU_AHBClk_GPIOA, ENABLE);
    RCU_AHBRstCmd(RCU_AHBRst_GPIOA, ENABLE);
    GPIO_OutCmd(GPIOA, GPIO_Pin_7_0, ENABLE);
    GPIO_DigitalCmd(GPIOA, GPIO_Pin_7_0, ENABLE);
    GPIO_SetBits(GPIOA, GPIO_Pin_7_0);
}

void ecap_init()
{
    // инициализация блока захвата
    RCU_APBClk0Cmd(RCU_APBClk0_ECAP2, ENABLE);
    RCU_APBRst0Cmd(RCU_APBRst0_ECAP2, ENABLE);

    // выбираем Capture режим
    ECAP_Init_TypeDef ECAP_InitStruct;
    ECAP_StructInit(&ECAP_InitStruct);
    ECAP_InitStruct.Mode = ECAP_Mode_Capture;
    ECAP_Init(ECAP2, &ECAP_InitStruct);

    // настраиваем Capture режим
    ECAP_Capture_Init_TypeDef ECAP_Capture_InitStruct;
    ECAP_Capture_StructInit(&ECAP_Capture_InitStruct);
    ECAP_Capture_InitStruct.Mode = ECAP_Capture_Mode_Cycle;
    ECAP_Capture_InitStruct.PolarityEvt0 = ECAP_Capture_Polarity_PosEdge;
    ECAP_Capture_InitStruct.RstEvt0 = ENABLE;
    ECAP_Capture_Init(ECAP2, &ECAP_Capture_InitStruct);

    // включаем прерывание по событию, чтобы считывать флаги
    ECAP_ITCmd(ECAP2, ECAP_ITSource_CapEvt0, ENABLE);

    // инициализация выводов блоков ECAP2
    RCU_AHBClkCmd(RCU_AHBClk_GPIOB, ENABLE);
    RCU_AHBRstCmd(RCU_AHBRst_GPIOB, ENABLE);
    GPIO_Init_TypeDef GPIO_InitStruct;
    GPIO_StructInit(&GPIO_InitStruct);
    GPIO_InitStruct.AltFuncNum = GPIO_AltFuncNum_1;
    GPIO_InitStruct.AltFunc = ENABLE;
    GPIO_InitStruct.Pin = GPIO_Pin_2;
    GPIO_Init(GPIOB, &GPIO_InitStruct);
    GPIO_DigitalCmd(GPIOB, GPIO_Pin_2, ENABLE);
}

void periph_init()
{
    SystemCoreClockUpdate();
    retarget_init();
    gpio_init();
    ecap_init();
    printf("\nAll peripherals inited, SYSCLK = %3d MHz\n", (int)(SystemCoreClock / 1E6));
}

//-- Main ----------------------------------------------------------------------
int main()
{
    periph_init();
    SysTick_Config(SystemCoreClock / 1000 - 1); //1ms tick
    printf("External signal frequency:\n");
    ECAP_TimerCmd(ECAP2, ENABLE);
    ECAP_Capture_Cmd(ECAP2, ENABLE);

    while (1) {
        if (ECAP_ITStatus(ECAP2, ECAP_ITStatus_CapEvt0)) {
            ECAP_ITStatusClear(ECAP2, ECAP_ITStatus_CapEvt0);
            ECAP_ITStatusClear(ECAP2, ECAP_ITStatus_GeneralInt);
            ECAP_ITPendStatusClear(ECAP2);
            printf("%d Hz     \r", (int)(APB0BusClock / ECAP_Capture_GetCap0(ECAP2)));
            for (uint32_t i = 0; i < 100000; i++)
                __NOP();
        }
    };
    return 0;
}

//-- IRQ handlers --------------------------------------------------------------
volatile uint32_t tick_counter = 0;
void SysTick_Handler()
{
    if (tick_counter >= 200) {
        tick_counter = 0;
        GPIO_ToggleBits(GPIOA, GPIO_Pin_0);
    } else
        tick_counter++;
}

//-- Assert --------------------------------------------------------------------
#if defined USE_FULL_ASSERT
void assert_failed(uint8_t* file, uint32_t line)
{
    printf("Assert failed: file %s on line %d\n", file, (int)line);
    while (1) {
    };
}
#endif /* USE_FULL_ASSERT */
